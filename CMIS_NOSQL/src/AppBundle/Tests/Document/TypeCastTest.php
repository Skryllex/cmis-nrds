<?php
namespace AppBundle\Tests\Document;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\DataFixtures\Fixtures\MockTypeCast;
use AppBundle\Enum\Service;
use SylrSyksSoftSymfony\CoreBundle\Enum\SylrSyksSoftServices;

final class TypeCastTest extends WebTestCase
{

    /**
     * Save.
     * @test
     */
    public function save()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $mock_type_cast = new MockTypeCast();
        $casting = $mock_type_cast->get();

        if (! empty($casting)) {
            $master_table_manager = $container->get(SylrSyksSoftServices::ManagerMasterTable);
            $dm = $container->get(Service::DoctrineMongoDBIdentifier)->getManager();
            foreach ($casting as $type_cast) {
                $builder = $container->get(Service::BuilderTypeCastBuilder);
                $object = $builder->build($type_cast['code'], $type_cast['title'], $type_cast['description']);
                if (!empty($type_cast['translation']) && method_exists($builder, 'addTranslation')) {
                    $is_entity = FALSE;
                    $builder->addTranslation($object, $dm, $type_cast['translation'], $is_entity);
                }

                $and_flush = ($type_cast === end($casting)) ? TRUE : FALSE;

                ld($type_cast);

                ld($and_flush);

//                 $dm->persist($object);
                $master_table_manager->save($object, $and_flush);
            }

//             $dm->flush();
        }
    }
}