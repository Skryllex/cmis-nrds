<?php
namespace AppBundle\Model;

use SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface;

interface ContentMediaCreditInterface extends ModelInterface
{
    /**
     * Set name
     *
     * @param string $name
     *      Name.
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Set surname
     *
     * @param string $surname
     *      Surname.
     */
    public function setSurname($surname = NULL);

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname();

    /**
     * Get full name
     *
     * @return string
     */
    public function getFullName();

    /**
     * Set biography
     *
     * @param string $biography
     *      Biography.
     */
    public function setBiography($biography = NULL);

    /**
     * Get biography
     *
     * @return string $biography.
     */
    public function getBiography();

    /**
     * Get slug.
     *
     * @return string $slug.
     */
    public function getSlug();
}