<?php
namespace AppBundle\Model;

use SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface;

interface ContentMediaInterface extends ModelInterface
{

    /**
     * Set title;
     *
     * @param string $title
     *            Title.
     */
    public function setTitle($title);

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set synopsis
     *
     * @param string $synopsis
     *            Synopsis.
     */
    public function setSynopsis($synopsis);

    /**
     * Get synopsis
     *
     * @return string
     */
    public function getSynopsis();

    /**
     * Set year
     *
     * @param string $year
     *            Year.
     */
    public function setYear($year);

    /**
     * Get year
     *
     * @return string
     */
    public function getYear();

    /**
     * Set country
     *
     * @param string $country
     *            Country;
     */
    public function setCountry($country);

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry();

    /**
     * Get slug.
     *
     * @return string $slug.
     */
    public function getSlug();
}