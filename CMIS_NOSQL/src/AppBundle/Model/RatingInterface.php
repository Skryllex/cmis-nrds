<?php

namespace AppBundle\Model;

use SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface;
use AppBundle\Model\VoteInterface;

interface RatingInterface extends ModelInterface
{
    /**
     * Set num votes
     *
     * @param integer $numVotes
     * @return RatingInterface
     */
    public function setNumVotes($numVotes);

    /**
     * Return num votes
     *
     * @return integer
     */
    public function getNumVotes();

    /**
     * Set the rate of the votes
     *
     * @param integer $rate
     * @return RatingInterface
     */
    public function setRate($rate);

    /**
     * Get the rate of the votes
     *
     * @return integer
     */
    public function getRate();

    /**
     * Set the permalink of the page
     *
     * @param string $permalink
     * @return RatingInterface
     */
    public function setPermalink($permalink);

    /**
     * Get the permalink of the page
     *
     * @return string
     */
    public function getPermalink();

    /**
     * Set the securityRole
     *
     * @param string $securityRole
     * @return RatingInterface
     */
    public function setSecurityRole($securityRole);

    /**
     * Get the securityRole
     *
     * @return string
     */
    public function getSecurityRole();

    /**
     * Add votes.
     *
     * @param VoteInterface $vote
     *      Vote.
     */
    public function addVote(VoteInterface $vote);

    /**
     * Remove vote.
     */
    public function removeVote(VoteInterface $vote);

    /**
     * Get votes.
     */
    public function getVotes();
}
