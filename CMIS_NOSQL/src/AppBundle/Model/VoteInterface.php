<?php

namespace AppBundle\Model;

use AppBundle\Model\RatingInterface;
use SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface;
use Symfony\Component\Security\Core\User\UserInterface;

interface VoteInterface extends ModelInterface
{

    /**
     * Set value
     *
     * @param integer $value
     * @return VoteInterface
     */
    public function setValue($value);

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue();

    /**
     * Set rating
     *
     * @param \DCS\RatingBundle\Model\RatingInterface $rating
     * @return VoteInterface
     */
    public function setRating(RatingInterface $rating);

    /**
     * Get rating
     *
     * @return \DCS\RatingBundle\Model\RatingInterface
     */
    public function getRating();

    /**
     * Sets the owner of the vote
     *
     * @param UserInterface $user
     * @return SignedVoteInterface
     */
    public function setVoter(UserInterface $voter);

    /**
     * Gets the owner of the vote
     *
     * @return UserInterface
     */
    public function getVoter();
}
