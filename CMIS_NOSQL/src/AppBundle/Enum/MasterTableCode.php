<?php
/**
 * @file
 *
 * MasterTable Enums.
 *
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class MasterTableCode extends Enum
{

    const Genre = 'GENR-';

    const Group = 'GRP-';

    const Quality = 'QUA-';

    const Season = 'SEASN-';

    const StateSeries = 'STSR-';

    const TypeBroadcastPlatform = 'TBCP-';

    const TypeCast = 'CAST-';

    const TypeRepeatViewing = 'TRV-';
}