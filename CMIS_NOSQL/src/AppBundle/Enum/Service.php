<?php
/**
 * @file
 * Common Enums.
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class Service extends Enum
{

    const SecurityContextIdentifier = 'security.context';

    const UserBuilder = 'cmis_nosql.builder.user';

    const BuilderUserBuilder = 'cmis_nosql.builder.user.builder';

    const CastBuilder = 'cmis_nosql.builder.cast';

    const BuilderContentMediaCreditCastBuilder = 'cmis_nosql.builder.content_media_credit.cast.builder';

    const MovieBuilder = 'cmis_nosql.builder.movie';

    const BuilderContentMediaMovieBuilder = 'cmis_nosql.builder.content_media.movie.builder';

    const CharacterBuilder = 'cmis_nosql.builder.character';

    const BuilderContentMediaCreditCharacterBuilder = 'cmis_nosql.builder.content_media_credit.character.builder';

    const TypeCastBuilder = 'cmis_nosql.builder.master_table.type_cast';

    const BuilderTypeCastBuilder = 'cmis_nosql.builder.master_table.builder.type_cast';

    const BuilderGenreBuilder = 'cmis_nosql.builder.master_table.builder.genre';

    const BuilderTypeBroadcastPlatformBuilder = 'cmis_nosql.builder.master_table.builder.type_broadcast_platform';

    const BuilderTypeRepeatViewingBuilder = 'cmis_nosql.builder.master_table.builder.type_repeat_viewing';

    const BuilderQualityBuilder = 'cmis_nosql.builder.master_table.builder.quality';

    const BuilderSeasonBuilder = 'cmis_nosql.builder.master_table.builder.season';

    const BuilderStateSeriesBuilder = 'cmis_nosql.builder.master_table.builder.state_series';

    const UserManager = 'cmis_nosql.manager.user';

}
