<?php
/**
 * @file
 *
 * User Enums.
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class Role extends Enum
{

    const __default = self::AuthenticatedUser;

    const AuthenticatedUser = "ROLE_AUTHENTICATED_USER";

    const AnonymousUser = "ROLE_ANONYMOUS_USER";

    const AllowedSwitch = 'ROLE_ALLOWED_TO_SWITCH';

    const Administrator = 'ROLE_USER';

    const SuperAdministrator = 'ROLE_SUPER_ADMIN';
}
