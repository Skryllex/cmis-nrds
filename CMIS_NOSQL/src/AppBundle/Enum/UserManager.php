<?php
/**
 * @file
 *
 * User Enums.
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class UserManager extends Enum
{

    const __default = self::FOSUserManager;

    const FOSUserManager = 'fos_user.user_manager';
    const SonatUserManager = 'sonata.user.mongodb.user_manager';
}