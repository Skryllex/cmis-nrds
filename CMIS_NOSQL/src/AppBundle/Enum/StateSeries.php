<?php
/**
 * @file
 *
 * TypeCast Enums.
 *
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class StateSeries extends Enum
{

    const __default = self::InBroadcasting;

    const CanceledBroadcasting = MasterTableCode::StateSeries . 'CB';

    const FinishedBroadcasting = MasterTableCode::StateSeries . 'FB';

    const InBroadcasting = MasterTableCode::StateSeries . 'IB';

    const PendingBroadcasting = MasterTableCode::StateSeries . 'PB';

    const WaitingNewSeason = MasterTableCode::StateSeries . 'WS';
}