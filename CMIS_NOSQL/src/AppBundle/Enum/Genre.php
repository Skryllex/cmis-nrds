<?php
/**
 * @file
 *
 * TypeCast Enums.
 *
 */
namespace AppBundle\Enum;

use AppBundle\Enum\MasterTableCode;
use MyCLabs\Enum\Enum;

final class Genre extends Enum
{

    const __default = self::Animation;

    const Action = MasterTableCode::Genre . 'AC';

    const Adventure = MasterTableCode::Genre . 'AD';

    const Animation = MasterTableCode::Genre . 'AN';

    const Comedy = MasterTableCode::Genre . 'CO';

    const Drama = MasterTableCode::Genre . 'DR';

    const Fantasy = MasterTableCode::Genre . 'FA';

    const Terror = MasterTableCode::Genre . 'HT';

    const Thriller = MasterTableCode::Genre . 'TH';

    const Reality = MasterTableCode::Genre . 'RE';

    const ScienceFiction = MasterTableCode::Genre . 'SF';

    const Western = MasterTableCode::Genre . 'WE';
}