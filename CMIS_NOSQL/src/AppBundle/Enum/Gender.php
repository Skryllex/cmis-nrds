<?php
/**
 * @file
 *
 * Gender Enum.
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class Gender extends Enum
{

    const __default = self::Unknown;

    const Unknown = 'u';

    const Male = 'm';

    const Female = 'f';
}
