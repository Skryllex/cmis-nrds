<?php
/**
 * @file
 *
 * User Enums.
 */
namespace AppBundle\Enum;

use AppBundle\Enum\Role;
use MyCLabs\Enum\Enum;

final class Group extends Enum
{

    const __default = self::Authenticated;

    const SuperAdministrator = array(
        Role::SuperAdministrator,
        Role::Administrator,
        Role::AllowedSwitch,
        Role::AuthenticatedUser
    );

    const Administrator = array(
        Role::Administrator,
        Role::AllowedSwitch,
        Role::AuthenticatedUser
    );

    const Authenticated = array(
        Role::AuthenticatedUser
    );
}
