<?php
namespace AppBundle\Form\ContentMedia\Movie\Type;

use SylrSyksSoftSymfony\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

final class DeleteMovieType extends AbstractType
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Form\ContentMedia\Type\CreateFormType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $builder->add("id", HiddenType::class);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Form\AbstractType::getName()
     */
    public function getName()
    {
        return 'cmis_nosql_form_movie_delete';
    }
}