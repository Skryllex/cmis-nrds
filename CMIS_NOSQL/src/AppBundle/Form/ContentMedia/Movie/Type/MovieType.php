<?php
namespace AppBundle\Form\ContentMedia\Movie\Type;

use AppBundle\Form\ContentMedia\Type\ContentMediaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class MovieType extends ContentMediaType
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Form\ContentMedia\Type\CreateFormType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $builder->add("duration", TextType::class, array(
            'label' => 'form.duration',
            'render_required_asterisk' => TRUE,
            'horizontal_input_wrapper_class' => 'col-md-2',
            'widget_addon_append' => array(
                'text' => 'min'
            ),
            'attr' => array(
                'size' => '25'
            )
        ));
//             ->add('genres')
//             ->add('casts')
//             ->add('characters')
//             ->add('broadcastPlatforms')
//             ->add('ratings');
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Form\AbstractType::getName()
     */
    public function getName()
    {
        return 'cmis_nosql_form_movie';
    }
}