<?php
/**
 * @file
 * Create form.
 */
namespace AppBundle\Form\ContentMedia\Type;

use SylrSyksSoftSymfony\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ContentMediaType extends AbstractType
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("title", TextType::class, array(
            'label' => 'form.title',
            'render_required_asterisk' => TRUE,
        ))
            ->add("synopsis", TextareaType::class, array(
            'label' => 'form.synopsis',
            'render_required_asterisk' => TRUE,
        ))
            ->add("year", TextType::class, array(
            'label' => 'form.year',
            'render_required_asterisk' => TRUE,
            'horizontal_input_wrapper_class' => 'col-md-2',
            'attr' => array(
                'size' => '4'
            ),
        ))
            ->add('country', CountryType::class, array(
            'label' => 'form.country',
            'render_required_asterisk' => TRUE,
        ));
    }
}