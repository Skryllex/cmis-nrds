<?php
/**
 * @file
 * Overrides Profile Form.
 */
namespace AppBundle\Form\User\Type;

use Sonata\UserBundle\Form\Type\ProfileType as SonataUserProfileFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ProfileFormType extends SonataUserProfileFormType
{

    private $defaultMergeOptions = array(
        'label_render' => FALSE,
        'widget_form_group_attr' => array(
            'class' => 'form-group box-wrap'
        ),
        'horizontal_input_wrapper_class' => 'col-sm-12',
        'horizontal_label_offset_class' => '',
        'translation_domain' => 'SonataUserBundle'
    );

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('username')
            ->remove('email')
            ->remove('current_password');

        $builder->add('username', null, array_merge(array(
            'widget_addon_prepend' => array(
                'icon' => 'user'
            ),
            'render_required_asterisk' => TRUE,
            'attr' => array(
                'placeholder' => 'base_login_input_username_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('email', 'email', array_merge(array(
            'widget_addon_prepend' => array(
                'icon' => 'envelope'
            ),
            'attr' => array(
                'placeholder' => 'registration_email_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('current_password', 'password', array_merge(array(
            'widget_addon_prepend' => array(
                'icon' => 'key'
            ),
            'mapped' => FALSE,
            'required' => FALSE,
            'attr' => array(
                'placeholder' => 'base_login_input_password_placeholder'
            ),
            'constraints' => new UserPassword()
        ), $this->defaultMergeOptions))
            ->add('firstname', 'text', array_merge(array(
            'widget_addon_prepend' => array(
                'icon' => 'user'
            ),
            'attr' => array(
                'placeholder' => 'registration_firstname_placeholder'
            ),
            'required' => FALSE
        ), $this->defaultMergeOptions))
            ->add('lastname', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'user'
            ),
            'attr' => array(
                'placeholder' => 'registration_lastname_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('dateOfBirth', 'birthday', array_merge(array(
            'widget' => 'single_text',
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'birthday-cake'
            ),
            'attr' => array(
                'placeholder' => 'registration_date_of_birth_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('gender', 'sonata_user_gender', array_merge(array(
            'required' => FALSE,
            'empty_value' => 'registration_gender_placeholder',
            'empty_data' => NULL,
            'attr' => array(
                'placeholder' => 'registration_gender_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('biography', 'textarea', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'keyboard-o'
            ),
            'attr' => array(
                'placeholder' => 'edit_profile_biography_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('website', 'url', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'external-link'
            ),
            'attr' => array(
                'placeholder' => 'edit_profile_website_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('phone', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'phone'
            ),
            'attr' => array(
                'placeholder' => 'registration_phone_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('address', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'building-o'
            ),
            'attr' => array(
                'placeholder' => 'registration_address_placeholder'
            ),
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 100
                ))
            )
        ), $this->defaultMergeOptions))
            ->add('postcode', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'location-arrow'
            ),
            'attr' => array(
                'placeholder' => 'registration_postcode_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('city', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'building'
            ),
            'attr' => array(
                'placeholder' => 'registration_city_placeholder'
            ),
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 50
                ))
            )
        ), $this->defaultMergeOptions))
            ->add('country', 'country', array_merge(array(
            'required' => FALSE,
            'empty_value' => 'registration_country_placeholder',
            'empty_data' => NULL,
            'attr' => array(
                'placeholder' => 'registration_country_placeholder'
            ),
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 10
                ))
            )
        ), $this->defaultMergeOptions))
            ->add('locale', 'locale', array_merge(array(
            'required' => FALSE,
            'empty_value' => 'edit_profile_locale_placeholder',
            'empty_data' => NULL,
            'widget_addon_prepend' => array(
                'icon' => 'location-arrow'
            ),
            'attr' => array(
                'placeholder' => 'edit_profile_locale_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('timezone', 'timezone', array_merge(array(
            'required' => FALSE,
            'empty_value' => 'edit_profile_timezone_placeholder',
            'empty_data' => NULL,
            'widget_addon_prepend' => array(
                'icon' => 'clock-o'
            ),
            'attr' => array(
                'placeholder' => 'edit_profile_timezone_placeholder'
            )
        ), $this->defaultMergeOptions));
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::getParent()
     */
    public function getParent()
    {
        return 'fos_user_profile';
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\UserBundle\Form\Type\ProfileType::getName()
     */
    public function getName()
    {
        return 'cmis_nosql_user_profile';
    }
}