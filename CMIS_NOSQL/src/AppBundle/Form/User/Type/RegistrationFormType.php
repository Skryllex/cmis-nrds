<?php
/**
 * @file
 * Overrides Registration Form.
 */
namespace AppBundle\Form\User\Type;

use Sonata\UserBundle\Form\Type\RegistrationFormType as SonataUserRegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegistrationFormType extends SonataUserRegistrationFormType
{

    private $defaultMergeOptions = array(
        'label_render' => FALSE,
        'widget_form_group_attr' => array(
            'class' => 'form-group box-wrap'
        ),
        'horizontal_input_wrapper_class' => 'col-sm-12',
        'horizontal_label_offset_class' => '',
        'translation_domain' => 'SonataUserBundle'
    );

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->mergeOptions = array_merge($this->mergeOptions, $this->defaultMergeOptions);

        $builder->remove('username')
            ->remove('email')
            ->remove('plainPassword');

        $general = $builder->create('general', 'tab', array(
            'label' => 'General',
            'icon' => 'pencil',
            'inherit_data' => TRUE
        ));

        $profile = $builder->create('profile', 'tab', array(
                'label' => 'Profile',
                'icon' => 'user',
                'inherit_data' => TRUE
            ));

        $general->add('username', TextType::class, array_merge(array(
            'widget_addon_prepend' => array(
                'icon' => 'user'
            ),
            'render_required_asterisk' => TRUE,
            'attr' => array(
                'placeholder' => 'base_login_input_username_placeholder'
            )
        ), $this->mergeOptions))
            ->add('email', 'email', array_merge(array(
            'widget_addon_prepend' => array(
                'icon' => 'envelope'
            ),
            'attr' => array(
                'placeholder' => 'registration_email_placeholder'
            )
        ), $this->mergeOptions))
            ->add('plainPassword', 'repeated', array_merge(array(
            'type' => 'password',
            'options' => array(
                'translation_domain' => 'SonataUserBundle'
            ),
            'first_options' => array_merge(array(
                'widget_addon_prepend' => array(
                    'icon' => 'key'
                ),
                'attr' => array(
                    'placeholder' => 'base_login_input_password_placeholder'
                )
            ), $this->mergeOptions),
            'second_options' => array_merge(array(
                'widget_addon_prepend' => array(
                    'icon' => 'repeat'
                ),
                'attr' => array(
                    'placeholder' => 'registration_confirm_password_placeholder'
                )
            ), $this->mergeOptions),
            'invalid_message' => 'fos_user.password.mismatch'
        ), $this->mergeOptions));

        $now = new \DateTime();
        $profile->add('firstname', 'text', array_merge(array(
            'widget_addon_prepend' => array(
                'icon' => 'user'
            ),
            'attr' => array(
                'placeholder' => 'registration_firstname_placeholder'
            ),
            'required' => FALSE
        ), $this->mergeOptions))
            ->add('lastname', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'user'
            ),
            'attr' => array(
                'placeholder' => 'registration_lastname_placeholder'
            )
        ), $this->mergeOptions))
            ->add('dateOfBirth', 'sonata_type_date_picker', array_merge(array(
            'years' => range(1900, $now->format('Y')),
            'dp_min_date' => '1-1-1900',
            'dp_max_date' => $now->format('c'),
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'birthday-cake'
            ),
            'attr' => array(
                'placeholder' => 'registration_date_of_birth_placeholder'
            )
        ), $this->mergeOptions))
            ->add('gender', 'sonata_user_gender', array_merge(array(
            'required' => FALSE,
            'empty_value' => 'registration_gender_placeholder',
            'empty_data' => NULL,
            'attr' => array(
                'placeholder' => 'registration_gender_placeholder'
            )
        ), $this->mergeOptions))
            ->add('phone', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'phone'
            ),
            'attr' => array(
                'placeholder' => 'registration_phone_placeholder'
            )
        ), $this->mergeOptions))
            ->add('address', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'building-o'
            ),
            'attr' => array(
                'placeholder' => 'registration_address_placeholder'
            ),
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 100
                ))
            )
        ), $this->mergeOptions))
            ->add('postcode', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'location-arrow'
            ),
            'attr' => array(
                'placeholder' => 'registration_postcode_placeholder'
            )
        ), $this->defaultMergeOptions))
            ->add('city', 'text', array_merge(array(
            'required' => FALSE,
            'widget_addon_prepend' => array(
                'icon' => 'building'
            ),
            'attr' => array(
                'placeholder' => 'registration_city_placeholder'
            ),
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 50
                ))
            )
        ), $this->mergeOptions))
            ->add('country', 'country', array_merge(array(
            'required' => FALSE,
            'empty_value' => 'registration_country_placeholder',
            'empty_data' => NULL,
            'attr' => array(
                'placeholder' => 'registration_country_placeholder'
            ),
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 10
                ))
            )
        ), $this->mergeOptions));

        $builder->add($general)->add($profile);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::getParent()
     */
    public function getParent()
    {
        return 'fos_user_registration';
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\UserBundle\Form\Type\RegistrationFormType::getName()
     */
    public function getName()
    {
        return 'cmis_nosql_user_registration';
    }
}