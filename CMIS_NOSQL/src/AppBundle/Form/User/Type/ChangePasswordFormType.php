<?php
namespace AppBundle\Form\User\Type;

use FOS\UserBundle\Form\Type\ChangePasswordFormType as FOSChangePasswordFormType;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Form\FormBuilderInterface;

class ChangePasswordFormType extends FOSChangePasswordFormType
{

    private $defaultMergeOptions = array(
        'label_render' => FALSE,
        'widget_form_group_attr' => array(
            'class' => 'form-group box-wrap'
        ),
        'horizontal_input_wrapper_class' => 'col-sm-12',
        'horizontal_label_offset_class' => '',
        'translation_domain' => 'SonataUserBundle'
    );

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('current_password')->remove('new');

        $builder->add('current_password', 'password', array_merge(array(
            'widget_addon_prepend' => array(
                'icon' => 'key'
            ),
            'mapped' => FALSE,
            'attr' => array(
                'placeholder' => 'change_password_current_password_placeholder'
            ),
            'constraints' => new UserPassword()
        ), $this->defaultMergeOptions))->add('new', 'repeated', array_merge(array(
            'type' => 'password',
            'options' => array(
                'translation_domain' => 'SonataUserBundle'
            ),
            'first_options' => array_merge(array(
                'widget_addon_prepend' => array(
                    'icon' => 'key'
                ),
                'attr' => array(
                    'placeholder' => 'change_password_new_password_placeholder'
                )
            ), $this->defaultMergeOptions),
            'second_options' => array_merge(array(
                'widget_addon_prepend' => array(
                    'icon' => 'repeat'
                ),
                'attr' => array(
                    'placeholder' => 'change_password_new_password_confirm_placeholder'
                )
            ), $this->defaultMergeOptions),
            'invalid_message' => 'fos_user.password.mismatch'
        ), $this->defaultMergeOptions));
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::getParent()
     */
    public function getParent()
    {
        return 'fos_user_change_password';
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::getBlockPrefix()
     */
    public function getBlockPrefix()
    {
        return 'cmis_nosql_change_password';
    }

    /**
     * Non-PHPdoc.
     *
     * @see \FOS\UserBundle\Form\Type\ProfileFormType::getName()
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}