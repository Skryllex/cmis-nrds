<?php
/**
 * @file
 * AppSonataUserBundle Bundle.
 */
namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppSonataUserBundle extends Bundle
{

    /**
     * Non-PHPdoc
     *
     * @see \Symfony\Component\HttpKernel\Bundle\Bundle::getParent()
     */
    public function getParent()
    {
        return 'SonataUserBundle';
    }
}
