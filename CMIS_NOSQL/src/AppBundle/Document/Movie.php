<?php
namespace AppBundle\Document;

use AppBundle\Document\Common\AbstractContentMedia;
use AppBundle\Enum\TypeCast as Code;
use AppBundle\Model\RatingInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Document Movie.
 * 
 * @package AppBundle\Document
 * @MongoDB\Document(collection="movies", indexes={
 *      @MongoDB\Index(name="movie_index1", keys={"title"="asc"}),
 *      @MongoDB\Index(name="movie_index2", keys={"year"="asc", "title"="asc"})
 * }, repositoryClass="AppBundle\Document\Repository\MovieRepository")
 */
final class Movie extends AbstractContentMedia
{
    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The duration field is required.")
     * @Assert\Regex(
     *      pattern="/[0-9]/",
     *      htmlPattern="[0-9]",
     *      match=true,
     *      message="The duration should contain numbers",
     *      groups={"ContentMedia"}
     * )
     */
    private $duration;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Gedmo\Slug(fields={"year", "title"})
     */
    private $slug;

    /* Relationships */

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Genre", inversedBy="movies", cascade={"persist", "merge", "detach", "refresh"})
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Genre")
     * @Assert\All({
     *      @Assert\Type("AppBundle\Document\Genre")
     * })
     */
    private $genres;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Cast", inversedBy="movies", cascade={"persist", "merge", "detach", "refresh"})
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Cast")
     * @Assert\All({
     *      @Assert\Type("AppBundle\Document\Cast")
     * })
     */
    private $casts;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Character", inversedBy="movies", cascade={"persist", "merge", "detach", "refresh"})
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Character")
     * @Assert\All({
     *      @Assert\Type("AppBundle\Document\Character")
     * })
     */
    private $characters;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\BroadcastPlatform", mappedBy="movies")
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\BroadcastPlatform")
     * @Assert\All({
     *      @Assert\Type("AppBundle\Document\BroadcastPlatform")
     * })
     */
    private $broadcastPlatforms;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Rating", inversedBy="movie", cascade={"all"})
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Rating")
     * @Assert\All({
     *      @Assert\Type("AppBundle\Document\Rating")
     * })
     */
    private $ratings;


    /**
     * Default contructor.
     */
    public function __construct()
    {
        $this->genres = new ArrayCollection();
        $this->casts = new ArrayCollection();
        $this->characters = new ArrayCollection();
        $this->broadcastPlatforms = new ArrayCollection();
        $this->ratings = new ArrayCollection();
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return Movie
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::getSlug()
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return Movie
     */
    public function addGenre(Genre $genre)
    {
        $this->genres[] = $genre;

        return $this;
    }

    /**
     * Remove genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGenre(Genre $genre)
    {
        return $this->genres->removeElement($genre);
    }

    /**
     * Get genres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Add cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return Movie
     */
    public function addCast(Cast $cast)
    {
        $this->casts[] = $cast;

        return $this;
    }

    /**
     * Remove cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCast(Cast $cast)
    {
        return $this->casts->removeElement($cast);
    }

    /**
     * Get casts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCasts()
    {
        return $this->casts;
    }

    /**
     * Get credit by code.
     *
     * @param string $code
     * @return array
     */
    public function getCredits($code = Code::__default)
    {
        $credits = array();
        $casts = $this->casts->toArray();

        if (!empty($casts)) {
            foreach ($casts as $cast) {
                $types = $cast->getTypesCast()->toArray();
                if (!empty($types)) {
                    foreach ($types as $type) {
                        if (strcmp($type->getCode(), $code) === 0) {
                            $credits[] = $cast;
                            break;
                        }
                    }
                }
            }
        }

        return $credits;
    }

    /**
     * Add character
     *
     * @param \AppBundle\Entity\Character $character
     *
     * @return Movie
     */
    public function addCharacter(Character $character)
    {
        $this->characters[] = $character;

        return $this;
    }

    /**
     * Remove character
     *
     * @param \AppBundle\Entity\Character $character
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCharacter(Character $character)
    {
        return $this->characters->removeElement($character);
    }

    /**
     * Get characters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Get broadcastPlatforms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBroadcastPlatforms()
    {
        return $this->broadcastPlatforms;
    }

    /**
     * Add rating
     *
     * @param \AppBundle\Model\RatingInterface $rating
     *
     * @return Movie
     */
    public function addRating(RatingInterface $rating)
    {
        $this->ratings[] = $rating;

        return $this;
    }

    /**
     * Remove rating
     *
     * @param \AppBundle\Model\RatingInterface $rating
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRating(RatingInterface $rating)
    {
        return $this->ratings->removeElement($rating);
    }

    /**
     * Get ratings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRatings()
    {
        return $this->ratings;
    }

}