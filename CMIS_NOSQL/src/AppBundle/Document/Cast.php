<?php
/**
 * @file
 * Document cast.
 */
namespace AppBundle\Document;

use AppBundle\Document\Common\AbstractContentMediaCredit;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @MongoDB\Document(collection="casts", indexes={
 *      @MongoDB\Index(name="cast_index1", keys={"name"="asc"}),
 *      @MongoDB\Index(name="cast_index2", keys={"name"="asc", "surname"="asc"})
 * })
 */
final class Cast extends AbstractContentMediaCredit
{
    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(nullable=true)
     * @Assert\DateTime()
     */
    private $birthDate;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(nullable=true)
     * @Assert\DateTime()
     */
    private $finishedDate;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\TypeCast", inversedBy="casts", cascade={"persist", "merge", "detach", "refresh"})
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\TypeCast")
     */
    private $typesCast;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Character", inversedBy="casts", cascade={"all"})
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Character")
     */
    private $characters;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Movie", mappedBy="casts")
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Movie")
     */
    private $movies;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->typesCast = new ArrayCollection();
        $this->characters = new ArrayCollection();
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return Cast
     */
    public function setBirthDate(\DateTime $birthDate = NULL)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set finishedDate
     *
     * @param \DateTime $finishedDate
     *
     * @return Cast
     */
    public function setFinishedDate(\DateTime $finishedDate = NULL)
    {
        $this->finishedDate = $finishedDate;

        return $this;
    }

    /**
     * Get finishedDate
     *
     * @return \DateTime
     */
    public function getFinishedDate()
    {
        return $this->finishedDate;
    }

    /**
     * Add type cast.
     *
     * @param \AppBundle\Document\TypeCast $typeCast
     *
     * @return Movie
     */
    public function addTypeCast(TypeCast $typeCast)
    {
        $this->typesCast[] = $typeCast;

        return $this;
    }

    /**
     * Remove type cast.
     *
     * @param \AppBundle\Document\TypeCast $typeCast
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTypeCast(TypeCast $typeCast)
    {
        return $this->casts->removeElement($typeCast);
    }

    /**
     * Get typesCast
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTypesCast()
    {
        return $this->typesCast;
    }

    /**
     * Add character
     *
     * @param \AppBundle\Document\Character $character
     *
     * @return Movie
     */
    public function addCharacter(Character $character)
    {
        $this->characters[] = $character;

        return $this;
    }

    /**
     * Remove character
     *
     * @param \AppBundle\Document\Character $character
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCharacter(Character $character)
    {
        return $this->characters->removeElement($character);
    }

    /**
     * Get characters
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Get movie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }
}