<?php
/**
 * @file
 * Document Quality.
 */
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableMasterTableDocument;

/**
 * @MongoDB\Document(collection="qualities", indexes={
 *      @MongoDB\Index(name="qualities_index", keys={"code"="asc", "title"="asc"}),
 * })
 */
final class Quality extends AbstractTranslatableMasterTableDocument
{
}
