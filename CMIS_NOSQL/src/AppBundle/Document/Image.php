<?php
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractFileDocument;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(collection="images")
 */
final class Image extends AbstractFileDocument
{
    /**
     *
     * @var UploadedFile
     *
     * @Assert\Image(maxSize="2M", mimeTypes={"image/png", "image/jpeg", "image/jpg"})
     */
    protected $file;
}