<?php
/**
 * @file
 * Document Season.
 */
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableMasterTableDocument;

/**
 * @MongoDB\Document(collection="seasons", indexes={
 *      @MongoDB\Index(name="seasons_index", keys={"code"="asc", "title"="asc"}),
 * })
 */
final class Season extends AbstractTranslatableMasterTableDocument
{
}
