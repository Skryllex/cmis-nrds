<?php
/**
 * @file
 * Document vote.
 */
namespace AppBundle\Document;

use AppBundle\Model\RatingInterface;
use AppBundle\Model\VoteInterface;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractDocument;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="votes")
 */
final class Vote extends AbstractDocument implements VoteInterface
{

    /**
     * Value of vote
     *
     * @var integer
     *
     * @MongoDB\Integer(nullable=false)
     */
    private $value;

    /**
     * Rating
     *
     * @var RatingInterface
     *
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\Rating", inversedBy="votes")
     */
    private $rating;

    /**
     * Rating
     *
     * @var \Symfony\Component\Security\Core\User\UserInterface
     *
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\User", inversedBy="votes")
     */
    private $voter;

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\VoteInterface::setValue()
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\VoteInterface::getValue()
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\VoteInterface::setRating()
     */
    public function setRating(RatingInterface $rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\VoteInterface::getRating()
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\VoteInterface::setVoter()
     */
    public function setVoter(UserInterface $voter)
    {
        $this->voter = $voter;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\VoteInterface::getVoter()
     */
    public function getVoter()
    {
        return $this->voter;
    }
}
