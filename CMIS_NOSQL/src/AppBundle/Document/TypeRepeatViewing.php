<?php
/**
 * @file
 * Entity TypeRepeatViewing.
 */
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableMasterTableDocument;

/**
 * @MongoDB\Document(collection="types_repeat_viewing", indexes={
 *      @MongoDB\Index(name="types_repeat_viewing_index", keys={"code"="asc", "title"="asc"}),
 * })
 */
final class TypeRepeatViewing extends AbstractTranslatableMasterTableDocument
{
}