<?php
/**
 * @file
 * Document Group.
 */
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\UserBundle\Document\BaseGroup;
use SylrSyksSoftSymfony\CoreBundle\Bundle\BundleInterface;
use SylrSyksSoftSymfony\CoreBundle\Bundle\BundleTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(collection="groups")
 * @MongoDB\UniqueIndex(name="group_unique_index", keys={"code"="asc","name"="asc"})
 * @Gedmo\Loggable()
 */
final class Group extends BaseGroup implements BundleInterface
{
    use BundleTrait;

    /**
     *
     * @var string
     *
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *
     * @var string
     *
     * @MongoDB\String(name="code", type="string", nullable=false)
     * @Gedmo\Versioned()
     * @Assert\NotBlank(message="The field is required.")
     * @Assert\Length(
     *      min=3,
     *      max=50,
     *      minMessage="The code is too short.",
     *      maxMessage="The code is too long."
     * )
     */
    private $code;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\Versioned()
     */
    protected $name;

    /**
     *
     * @var Collection
     *
     * @MongoDB\Collection(nullable=false)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\Versioned()
     */
    protected $roles;

    /**
     * Set code
     *
     * @param string $code
     * @return \AppBundle\Document\Group
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
}
