<?php
namespace AppBundle\Document;

use AppBundle\Document\Common\AbstractContentMediaCredit;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="characters", indexes={
 *      @MongoDB\Index(name="characters_index1", keys={"name"="asc"}),
 *      @MongoDB\Index(name="characters_index2", keys={"name"="asc", "surname"="asc"})
 * })
 */
final class Character extends AbstractContentMediaCredit
{
    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Cast", mappedBy="characters")
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Cast")
     */
    private $casts;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Movie", mappedBy="characters")
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Movie")
     */
    private $movies;

    /**
     * Get cast
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCasts()
    {
        return $this->casts;
    }

    /**
     * Get movie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }
}