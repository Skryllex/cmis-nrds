<?php
/**
 * @file
 * Document User.
 */
namespace AppBundle\Document;

use AppBundle\Document\Common\AbstractUser;
use AppBundle\Model\VoteInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(collection="users")
 * @Gedmo\Loggable()
 */
final class User extends AbstractUser
{

    /**
     * @var string
     *
     * @MongoDB\String(name="address", type="string", nullable=true)
     * @Assert\Length(
     *      max=100,
     *      maxMessage="The name is too long.",
     *      groups={"Registration", "Profile"}
     * )
     */
    private $address;

    /**
     * @var string
     *
     * @MongoDB\String(name="postcode", type="string", nullable=true)
     * @Assert\Length(
     *      max=10,
     *      maxMessage="The name is too long.",
     *      groups={"Registration", "Profile"}
     * )
     */
    private $postcode;

    /**
     * @var string
     *
     * @MongoDB\String(name="city", type="string", nullable=true)
     * @Assert\Length(
     *      max=50,
     *      maxMessage="The name is too long.",
     *      groups={"Registration", "Profile"}
     * )
     */
    private $city;

    /**
     * @var string
     *
     * @MongoDB\String(name="country", type="string", nullable=true)
     * @Assert\Length(
     *      max=50,
     *      maxMessage="The name is too long.",
     *      groups={"Registration", "Profile"}
     * )
     */
    private $country;

    /**
     * Votes
     *
     * @var AppBundle\Document\Vote
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Vote", mappedBy="voter")
     */
    private $votes;

    /**
     * Default constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->votes = new ArrayCollection();
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return User
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::addVote()
     */
    public function addVote(VoteInterface $vote)
    {
        $this->votes->add($vote);

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::removeVote()
     */
    public function removeVote(VoteInterface $vote)
    {
        $this->votes->remove($vote);

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::getVotes()
     */
    public function getVotes()
    {
        return $this->votes;
    }
}
