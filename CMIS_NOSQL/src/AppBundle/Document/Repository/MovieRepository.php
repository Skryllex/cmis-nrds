<?php
namespace AppBundle\Document\Repository;

use SylrSyksSoftSymfony\CoreBundle\Document\Repository\DocumentRepository;

final class MovieRepository extends DocumentRepository
{

    /**
     *
     * @param array $criteria
     *            Query criteria
     * @param array $sort
     *            Sort array for Cursor::sort()
     * @param integer|null $limit
     *            Limit for Cursor::limit()
     * @param integer|null $skip
     *            Skip for Cursor::skip()
     */
    public function findMovies(array $criteria, array $sort = NULL, $limit = NULL, $skip = NULL)
    {
        return $this->findBy($criteria, $sort, $limit, $skip);
    }
}