<?php
/**
 * @file
 * Document TypeBroadCastPlatform.
 */
namespace AppBundle\Document;

use AppBundle\Document\BroadcastPlatform;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableMasterTableDocument;

/**
 * @MongoDB\Document(collection="types_broadcast_platform", indexes={
 *      @MongoDB\Index(name="types_broadcast_platform_index", keys={"code"="asc", "title"="asc"}),
 * })
 */
final class TypeBroadCastPlatform extends AbstractTranslatableMasterTableDocument
{
    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\BroadcastPlatform", mappedBy="typeBroadcastPlatform")
     */
    private $broadcastPlatform;

    /**
     * Get broadcast platform.
     *
     * @return AppBundle\Document\BroadcastPlatform
     */
    public function getBroadcastPlatform()
    {
        return $this->broadcastPlatform;
    }

}