<?php
/**
 * @file
 * BasicUser.
 */
namespace AppBundle\Document\Common;

use AppBundle\Enum\Gender;
use AppBundle\Enum\Role;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Model\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Sonata\UserBundle\Document\BaseUser;
use SylrSyksSoftSymfony\CoreBundle\Bundle\BundleInterface;
use SylrSyksSoftSymfony\CoreBundle\Bundle\BundleTrait;
use SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\MappedSuperclass()
 * @MongoDB\UniqueIndex(name="user_unique_index", keys={"username"="asc","email"="asc"})
 * @Gedmo\Loggable()
 */
abstract class AbstractUser extends BaseUser implements BundleInterface, Translatable, ModelInterface
{
    use BundleTrait;

    /**
     * @var string
     *
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field username is required.")
     * @Gedmo\Versioned()
     */
    protected $username;

    /**
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field email is required.")
     * @Gedmo\Versioned()
     */
    protected $email;

    /**
     * Plain password.
     * Used for model validation. Must not be persisted.
     *
     * @var string
     *
     * @MongoDB\String()
     * @Assert\NotBlank(message="The field password is required.")
     */
    protected $plainPassword;

    /**
     * Encrypted password.
     * Must be persisted.
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Gedmo\Versioned()
     */
    protected $password;

    /**
     * @var string
     *
     * @MongoDB\String()
     * @Gedmo\Versioned()
     */
    protected $firstname;

    /**
     * @var string
     *
     * @MongoDB\String()
     * @Gedmo\Versioned()
     */
    protected $lastname;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date()
     * @Gedmo\Versioned()
     */
    protected $dateOfBirth;

    /**
     * @var string
     *
     * @MongoDB\String()
     * @Gedmo\Versioned()
     */
    protected $gender = Gender::Unknown;

    /**
     * @var string
     *
     * @MongoDB\String()
     * @Gedmo\Versioned()
     */
    protected $phone;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $website;

    /**
     * @var string
     *
     * @MongoDB\String()
     * @Gedmo\Translatable()
     */
    protected $biography;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $locale;

    /**
     * @var string
     *
     * @Gedmo\Locale()
     */
    protected $tranlatableLocale;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $timezone;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $facebookUid;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $facebookName;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $facebookData;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $twitterUid;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $twitterName;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $twitterData;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $gplusUid;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $gplusName;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $gplusData;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $token;

    /**
     * @var string
     *
     * @MongoDB\String(nullable=false)
     */
    protected $usernameCanonical;

    /**
     * @var string
     *
     * @MongoDB\String(nullable=false)
     */
    protected $emailCanonical;

    /**
     *
     * @var boolean @MongoDB\Bool(nullable=false)
     */
    protected $enabled;

    /**
     * The salt to use for hashing
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field salt is required.")
     */
    protected $salt;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date()
     */
    protected $lastLogin;

    /**
     * Random string sent to the user email address in order to verify it
     *
     * @var string
     *
     * @MongoDB\String()
     */
    protected $confirmationToken;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date()
     */
    protected $passwordRequestedAt;

    /**
     * @var array
     *
     * @MongoDB\Collection()
     */
    protected $groups;

    /**
     * @var array
     *
     * @MongoDB\Collection(nullable=false)
     * @Assert\NotBlank(message="The field roles is required.")
     */
    protected $roles;

    /**
     * @var bool
     *
     * @MongoDB\Bool(nullable=false)
     */
    protected $locked;

    /**
     * @var bool
     *
     * @MongoDB\Bool(nullable=false)
     */
    protected $expired;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date()
     */
    protected $expiresAt;

    /**
     * @var bool
     *
     * @MongoDB\Bool(nullable=false)
     */
    protected $credentialsExpired;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date()
     */
    protected $credentialsExpireAt;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date(nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date(nullable=false)
     */
    protected $updatedAt;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(nullable=false)
     */
    protected $deletedAt;

    /**
     * @var string
     *
     * @MongoDB\String()
     */
    protected $twoStepVerificationCode;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->addRole(Role::__default);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \FOS\UserBundle\Model\User::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the gender list.
     *
     * @return array
     */
    public static function getGenderList()
    {
        return array(
            Gender::Unknown => 'gender_unknown',
            Gender::Female => 'gender_female',
            Gender::Male => 'gender_male'
        );
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \FOS\UserBundle\Model\User::addRole()
     */
    public function addRole($role)
    {
        $role = strtoupper($role);

        if (! in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \FOS\UserBundle\Model\User::getGroups()
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Non-PHPdoc.
     *
     * @see \FOS\UserBundle\Model\User::getGroupNames()
     */
    public function getGroupNames()
    {
        return $this->groups;
    }

    /**
     * Non-PHPdoc.
     *
     * @see \FOS\UserBundle\Model\User::addGroup()
     */
    public function addGroup(GroupInterface $group)
    {
        $groups = (!empty($this->groups)) ? $this->groups : array();
        $groupRoles = $group->getRoles();

        if (is_array($groupRoles) && ! empty($groupRoles)) {
            $groups = array_merge($groups, $groupRoles);
            $groups = array_unique($groups);
        }

        $this->groups = $groups;
        $this->setRealRoles($this->groups);

        return $this;
    }

    /**
     * Non-PHPdoc.
     *
     * @see \FOS\UserBundle\Model\User::removeGroup()
     */
    public function removeGroup(GroupInterface $group)
    {
        if (false !== $key = array_search($group->getName(), $this->groups, TRUE)) {
            unset($this->groups[$key]);
            $this->groups = array_values($this->groups);
        }

        return $this;
    }

    /**
     * Non-PHPdoc.
     *
     * @see \FOS\UserBundle\Model\User::getRoles()
     */
    public function getRoles()
    {
        $roles = $this->roles;

        if (is_array($this->groups) && ! empty($this->groups)) {
            $roles = array_merge($roles, $this->groups);
        }

        return array_unique($roles);
    }

    /**
     * Set locale.
     *
     * @param string $locale
     * @return \AppBundle\Document\Common\BasicUser
     */
    public function setTranslatableLocale($locale)
    {
        $this->tranlatableLocale = $locale;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Sonata\UserBundle\Model\User::setCreatedAt()
     */
    public function setCreatedAt(\DateTime $createdAt = NULL)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Sonata\UserBundle\Model\User::getCreatedAt()
     */
    public function getCreatedAt()
    {
        $this->createdAt;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Sonata\UserBundle\Model\User::setUpdatedAt()
     */
    public function setUpdatedAt(\DateTime $updatedAt = NULL)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Sonata\UserBundle\Model\User::getUpdatedAt()
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface::setDeletedAt()
     */
    public function setDeletedAt(\DateTime $deletedAt = NULL)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }


    /**
     *
     * {@inheritDoc}
     *
     * @see \SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface::getDeletedAt()
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}