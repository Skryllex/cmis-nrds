<?php
/**
 * @file
 * Content media credit.
 */
namespace AppBundle\Document\Common;

use AppBundle\Model\ContentMediaCreditInterface;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableDocument;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\MappedSuperclass()
 * @Unique(fields={"name", "surname"})
 * @Gedmo\Loggable()
 */
abstract class AbstractContentMediaCredit extends AbstractTranslatableDocument implements ContentMediaCreditInterface
{
    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field is required.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The name is too long.",
     *      groups={"ContentMediaCredit"}
     * )
     * @Gedmo\Versioned()
     */
    protected $name;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=true)
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The title is too long.",
     *      groups={"ContentMediaCredit"}
     * )
     */
    protected $surname;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=true)
     * @Assert\Length(
     *      max=10000,
     *      maxMessage="The biography is too long.",
     *      groups={"ContentMediaCredit"}
     * )
     * @Gedmo\Translatable()
     */
    protected $biography;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\Slug(fields={"name", "surname"})
     */
    protected $slug;

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaCreditInterface::setName()
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaCreditInterface::getName()
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaCreditInterface::setSurname()
     */
    public function setSurname($surname = NULL)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaCreditInterface::getSurname()
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\ContentMediaCreditInterface::getFullName()
     */
    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaCreditInterface::setBiography()
     */
    public function setBiography($biography = NULL)
    {
        $this->biography = $biography;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaCreditInterface::getBiography()
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\ContentMediaCreditInterface::getSlug()
     */
    public function getSlug()
    {
        return $this->slug;
    }
}