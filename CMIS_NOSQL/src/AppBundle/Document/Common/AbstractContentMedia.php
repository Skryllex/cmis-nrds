<?php
/**
 * @file
 * ContentMedia.
 */
namespace AppBundle\Document\Common;

use AppBundle\Model\ContentMediaInterface;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableDocument;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\MappedSuperclass()
 * @Unique(fields={"title", "year", "country"})
 * @Gedmo\Loggable()
 */
abstract class AbstractContentMedia extends AbstractTranslatableDocument implements ContentMediaInterface
{

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The title field is required.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The title is too long.",
     *      groups={"ContentMedia"}
     * )
     * @Gedmo\Translatable()
     * @Gedmo\Versioned()
     */
    protected $title;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The synopsys field is required.")
     * @Assert\Length(
     *      max=10000,
     *      maxMessage="The synopsis is too long.",
     *      groups={"ContentMedia"}
     * )
     * @Gedmo\Translatable()
     */
    protected $synopsis;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The year field is required.")
     * @Assert\Regex(
     *      pattern="/[0-9]/",
     *      htmlPattern="[0-9]",
     *      match=true,
     *      message="The year should contain numbers",
     *      groups={"ContentMedia"}
     * )
     * @Assert\Length(
     *      max=4,
     *      maxMessage="The year is too long.",
     *      groups={"ContentMedia"}
     * )
     */
    protected $year;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The country field is required.")
     * @Assert\Length(
     *      max=2,
     *      maxMessage="The title is too long.",
     *      groups={"ContentMedia"}
     * )
     * @Assert\Country()
     */
    protected $country;

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::setTitle()
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::getTitle()
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::setSynopsis()
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::getSynopsis()
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::setYear()
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::getYear()
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::setCountry()
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\ContentMediaInterface::getCountry()
     */
    public function getCountry()
    {
        return $this->country;
    }
}