<?php
/**
 * @file
 * Defining class for GridFS.
 */

namespace AppBundle\Document\Common;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractDocument;

/**
 * @MongoDB\MappedSuperclass()
 * @Gedmo\Loggable()
 *
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/loggable.md
 * @link http://php-and-symfony.matthiasnoback.nl/2012/10/uploading-files-to-mongodb-gridfs-2/
 */
abstract class BasicFileGridFS extends AbstractDocument
{
    /**
     * @var string
     *
     * @MongoDB\String(name="file_name")
     */
    protected $filename;

    /**
     * @var string
     *
     * @MongoDB\String(name="mime_type")
     */
    protected $mimeType;

    /**
     * @var string
     *
     * @MongoDB\Date(name="upload_date")
     */
    protected $uploadDate;

    /**
     * @var int
     *
     * @MongoDB\Int()
     */
    protected $length;

    /**
     * @var int
     *
     * @MongoDB\Int(name="chunk_size")
     */
    protected $chunkSize;

    /**
     * @var string
     *
     * @MongoDB\String()
     * @Gedmo\Versioned()
     */
    protected $md5;

    /**
     * Set filename
     *
     * @param string $filename
     * @return @return \AppBundle\Document\Common\BasicFileGridFS
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * Get filename.
     * @return string $filename.
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set mimeType.
     *
     * @param string $mimeType
     * @return \AppBundle\Document\Common\BasicFileGridFS
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * Get mimeType
     * @return string $mimeType.
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Get chunkSize
     *
     * @return int $chunkSize.
     */
    public function getChunkSize()
    {
        return $this->chunkSize;
    }

    /**
     * Get length.
     *
     * @return int $length.
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Get md5.
     *
     * @return string $md5.
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * Get uploadDate.
     *
     * @return \DateTime $uploadDate.
     */
    public function getUploadDate()
    {
        return $this->uploadDate;
    }
}