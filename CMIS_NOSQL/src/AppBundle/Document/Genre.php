<?php
/**
 * @file
 * Document Genre.
 */
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableMasterTableDocument;

/**
 * @MongoDB\Document(collection="genres", indexes={
 *      @MongoDB\Index(name="genres_index", keys={"code"="asc", "title"="asc"}),
 * })
 */
final class Genre extends AbstractTranslatableMasterTableDocument
{
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Movie", mappedBy="genres")
     */
    private $movies;

    /**
     * Get movie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }
}
