<?php
/**
 * @file
 * Document Broadcast platform.
 */
namespace AppBundle\Document;

use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableDocument;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\MappedSuperclass()
 * @DoctrineAssert\UniqueEntity(fields={"name", "slug"})
 * @Unique(fields={"name"})
 * @Gedmo\Loggable()
 */
final class BroadcastPlatform extends AbstractTranslatableDocument
{

    /**
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field is required.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The name is too long.",
     *      groups={"Document"}
     * )
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @var string
     *
     * @MongoDB\String(nullable=true)
     * @Assert\Length(
     *      max=10000,
     *      maxMessage="The description is too long.",
     *      groups={"Document"}
     * )
     * @Gedmo\Translatable()
     */
    private $description;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=true)
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The title is too long.",
     *      groups={"ContentMedia"}
     * )
     * @Gedmo\Translatable()
     * @Gedmo\Versioned()
     */
    private $slogan;

    /**
     *
     * @var \DateTime
     *
     * @MongoDB\Date(nullable=true)
     * @Assert\DateTime()
     */
    private $startTransmission;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=true)
     * @Assert\NotBlank(message="The field is required.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The title is too long.",
     *      groups={"Document"}
     * )
     * @Assert\Url(message= "The url '{{ value }}' is not a valid url")
     */
    private $website;

    /**
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\Slug(fields={"name"}, prefix="content/platform/")
     */
    private $slug;

    /* Relationships */

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\TypeBroadCastPlatform", inversedBy="broadcastPlatform", cascade={"all"})
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\TypeBroadCastPlatform")
     */
    private $typesBroadcastPlatform;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Movie", inversedBy="broadcastPlatforms", cascade={"all"})
     * @MongoDB\EmbedMany(targetDocument="AppBundle\Document\Movie")
     */
    private $movies;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->typesBroadCastPlatform = new ArrayCollection();
        $this->movies = new ArrayCollection();
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return BroadcastPlatform
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return BroadcastPlatform
     */
    public function setDescription($description = NULL)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return BroadcastPlatform
     */
    public function setSlogan($slogan = NULL)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Set startTransmission
     *
     * @param \DateTime $startTransmission
     *
     * @return BroadcastPlatform
     */
    public function setStartTransmission(\DateTime $startTransmission = NULL)
    {
        $this->startTransmission = $startTransmission;

        return $this;
    }

    /**
     * Get startTransmission
     *
     * @return \DateTime
     */
    public function getStartTransmission()
    {
        return $this->startTransmission;
    }

    /**
     * Set webSite
     *
     * @param string $webSite
     *
     * @return BroadcastPlatform
     */
    public function setWebSite($webSite)
    {
        $this->webSite = $webSite;

        return $this;
    }

    /**
     * Get webSite
     *
     * @return string
     */
    public function getWebSite()
    {
        return $this->webSite;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add typesBroadCastPlatform
     *
     * @param \AppBundle\Document\TypeBroadCastPlatform $typesBroadCastPlatform
     *
     * @return BroadcastPlatform
     */
    public function addTypesBroadCastPlatform(TypeBroadCastPlatform $typesBroadCastPlatform)
    {
        $this->typesBroadCastPlatform[] = $typesBroadCastPlatform;

        return $this;
    }

    /**
     * Remove typesBroadCastPlatform
     *
     * @param \AppBundle\Document\TypeBroadCastPlatform $typesBroadCastPlatform
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTypesBroadCastPlatform(TypeBroadCastPlatform $typesBroadCastPlatform)
    {
        return $this->typesBroadCastPlatform->removeElement($typesBroadCastPlatform);
    }

    /**
     * Get typesBroadCastPlatform
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getTypesBroadCastPlatform()
    {
        return $this->typesBroadCastPlatform;
    }

    /**
     * Add movie
     *
     * @param \AppBundle\Document\\Movie $movie
     *
     * @return BroadcastPlatform
     */
    public function addMovies(Movie $movie)
    {
        $this->movies[] = $movie;

        return $this;
    }

    /**
     * Remove movie
     *
     * @param \AppBundle\Document\\Movie $movie
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMovies(Movie $movie)
    {
        return $this->movies->removeElement($movie);
    }

    /**
     * Get movies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }
}