<?php
/**
 * @file
 * Entity StateSeries.
 */
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableMasterTableDocument;

/**
 * @MongoDB\Document(collection="states_series", indexes={
 *      @MongoDB\Index(name="states_series_index", keys={"code"="asc", "title"="asc"}),
 * })
 */
final class StateSeries extends AbstractTranslatableMasterTableDocument
{
}
