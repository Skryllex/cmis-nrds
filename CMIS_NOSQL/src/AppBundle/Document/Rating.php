<?php
/**
 * @file
 * Document rating.
 */
namespace AppBundle\Document;

use AppBundle\Model\RatingInterface;
use AppBundle\Model\VoteInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractDocument;

/**
 * @MongoDB\Document(collection="ratings")
 */
final class Rating extends AbstractDocument implements RatingInterface
{

    /**
     * Total votes in a thread
     *
     * @var int
     *
     * @MongoDB\Int(name="num_votes", nullable=false)
     */
    private $numVotes;

    /**
     * Rate votes in a thread
     *
     * @var int
     *
     * @MongoDB\Int(nullable=false)
     */
    private $rate;

    /**
     * Base security role
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     */
    private $securityRole;

    /**
     * Url of the page where the thread lives
     *
     * @var string
     *
     * @MongoDB\String(nullable=false)
     */
    private $permalink;

    /**
     * Votes
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Vote", mappedBy="rating")
     */
    private $votes;

    /**
     * Votes
     *
     * @var \AppBundle\Document\Movie
     *
     * @MongoDB\ReferenceOne(targetDocument="AppBundle\Document\Movie", mappedBy="ratings")
     */
    private $movie;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->rate = 0;
        $this->numVotes = 0;
        $this->votes = new ArrayCollection();
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::setNumVotes()
     */
    public function setNumVotes($numVotes)
    {
        $this->numVotes = $numVotes;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::getNumVotes()
     */
    public function getNumVotes()
    {
        return $this->numVotes;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::setRate()
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get the rate of the votes
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::setPermalink()
     */
    public function setPermalink($permalink = null)
    {
        $this->permalink = $permalink;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::getPermalink()
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::setSecurityRole()
     */
    public function setSecurityRole($securityRole)
    {
        $this->securityRole = $securityRole;

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::getSecurityRole()
     */
    public function getSecurityRole()
    {
        return $this->securityRole;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::addVote()
     */
    public function addVote(VoteInterface $vote)
    {
        $this->votes->add($vote);

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::removeVote()
     */
    public function removeVote(VoteInterface $vote)
    {
        $this->votes->remove($vote);

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Model\RatingInterface::getVotes()
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Get movie.
     *
     * @return \AppBundle\Document\Movie
     */
    public function getMovie() {
        return $this->movie;
    }
}
