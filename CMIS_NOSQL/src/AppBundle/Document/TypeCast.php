<?php
/**
 * @file
 * Document Type cast.
 */
namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use SylrSyksSoftSymfony\CoreBundle\Document\AbstractTranslatableMasterTableDocument;

/**
 * @MongoDB\Document(collection="type_casts", indexes={
 *      @MongoDB\Index(name="type_casts_index", keys={"code"="asc", "title"="asc"}),
 * })
 */
final class TypeCast extends AbstractTranslatableMasterTableDocument
{
    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\TypeCast", mappedBy="typesCast")
     */
    private $casts;

    /**
     * Get broadcast platform.
     *
     * @return AppBundle\Document\Cast
     */
    public function getCasts()
    {
        return $this->casts;
    }
}