<?php
/**
 * @file
 * DataFixture Resolutions.
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Fixtures\MockQuality;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Resolutions implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    /**
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_quality = new MockQuality();
        $resolutions = $mock_quality->get();

        if (! empty($resolutions)) {
            $data = array();
            foreach ($resolutions as $resolution) {
                $builder = $this->container->get(Service::BuilderQualityBuilder);
                $object = $builder->build($resolution['code'], $resolution['title'], $resolution['description']);
                if (! empty($resolution['translation']) && method_exists($builder, 'setTranslation')) {
                    $object = $builder->setTranslation($object, $resolution['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Symfony\Component\DependencyInjection\ContainerAwareInterface::setContainer()
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 6;
    }
}