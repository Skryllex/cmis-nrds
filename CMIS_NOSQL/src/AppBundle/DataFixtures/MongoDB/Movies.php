<?php
namespace AppBundle\DataFixtures\MongoDB;

use AppBundle\DataFixtures\Fixtures\MockMovie;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

final class Movies implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_movie = new MockMovie();
        $movies = $mock_movie->get();

        if (! empty($movies)) {
            $data = array();
            foreach ($movies as $movie) {
                $builder = $this->container->get(Service::BuilderContentMediaMovieBuilder);
                $object = $builder->build($movie['title'], $movie['synopsis'], $movie['year'], $movie['country'], NULL, $movie['duration'], $movie['genres'], $movie['casts'], $movie['characters']);
                if (! empty($movie['translation']) && method_exists($builder, 'setTranslation')) {
                    $object = $builder->setTranslation($object, $movie['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 11;
    }
}