<?php
/**
 * @file
 * DataFixture RepeatsViewing.
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Fixtures\MockTypeRepeatViewing;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class RepeatsViewing implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_type_repeat_viewing = new MockTypeRepeatViewing();
        $viewings = $mock_type_repeat_viewing->get();

        if (! empty($viewings)) {
            $data = array();
            foreach ($viewings as $viewing) {
                $builder = $this->container->get(Service::BuilderTypeRepeatViewingBuilder);
                $object = $builder->build($viewing['code'], $viewing['title'], $viewing['description']);
                if (! empty($viewing['translation']) && method_exists($builder, 'setTranslation')) {
                    $object = $builder->setTranslation($object, $viewing['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 5;
    }
}