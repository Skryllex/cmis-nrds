<?php
/**
 * @file
 * DataFixture StateSeries.
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Fixtures\MockStateSeries;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class StatesSeries implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_state_series = new MockStateSeries();
        $state_series = $mock_state_series->get();

        if (! empty($state_series)) {
            $data = array();
            foreach ($state_series as $state) {
                $builder = $this->container->get(Service::BuilderStateSeriesBuilder);
                $object = $builder->build($state['code'], $state['title'], $state['description']);
                if (! empty($state['translation']) && method_exists($builder, 'setTranslation')) {
                    $builder->setTranslation($object, $state['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 8;
    }
}