<?php
namespace AppBundle\DataFixtures\MongoDB;

use AppBundle\DataFixtures\Fixtures\MockCast;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

final class Casts implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_cast = new MockCast();
        $casts = $mock_cast->get();

        if (! empty($casts)) {
            $data = array();
            foreach ($casts as $cast) {
                $builder = $this->container->get(Service::BuilderContentMediaCreditCastBuilder);
                $object = $builder->build($cast['name'], $cast['surname'], $cast['biography'], $cast['birth_date'], $cast['finished_date'], $cast['types_cast'], $cast['characters']);
                if (! empty($cast['translation']) && method_exists($builder, 'setTranslation')) {
                    $object = $builder->setTranslation($object, $cast['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 10;
    }
}