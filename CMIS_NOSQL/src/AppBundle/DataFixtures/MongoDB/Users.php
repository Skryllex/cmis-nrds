<?php
/**
 * @file
 * DataFixture Roles.
 */
namespace AppBundle\DataFixtures\MongoDB;

use AppBundle\DataFixtures\Fixtures\MockUser;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Users implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     * Non-PHPdoc.
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_user = new MockUser();
        $users = $mock_user->get();

        if (! empty($users)) {
            $data = array();
            foreach ($users as $user) {
                $builder = $this->container->get(Service::BuilderUserBuilder);
                $date = \DateTime::createFromFormat("Y-m-d", $user['birth_date']);
                $object = $builder->build($user['username'], $user['email'], $user['password'], $user['firstname'], $user['lastname'], $date, $user['gender'], $user['address'], $user['postcode'], $user['city'], $user['country'], $user['website'], $user['enabled'], $user['roles'], $user['groups']);
                if (! empty($user['translation']) && method_exists($builder, 'setTranslation')) {
                    $object = $builder->setTranslation($object, $user['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 9;
    }
}