<?php
/**
 * @file
 * DataFixture Seasons.
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Fixtures\MockSeason;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Seasons implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_season = new MockSeason();
        $seasons = $mock_season->get();

        if (! empty($seasons)) {
            $data = array();
            foreach ($seasons as $season) {
                $builder = $this->container->get(Service::BuilderSeasonBuilder);
                $object = $builder->build($season['code'], $season['title'], $season['description']);
                if (! empty($season['translation']) && method_exists($builder, 'setTranslation')) {
                    $builder->setTranslation($object, $season['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 7;
    }
}