<?php
/**
 * @file
 * DataFixture Casting.
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Fixtures\MockTypeCast;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class Casting implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_genre = new MockTypeCast();
        $types = $mock_genre->get();

        if (! empty($types)) {
            $data = array();
            foreach ($types as $type) {
                $builder = $this->container->get(Service::BuilderTypeCastBuilder);
                $object = $builder->build($type['code'], $type['title'], $type['description']);
                if (! empty($type['translation']) && method_exists($builder, 'setTranslation')) {
                    $object = $builder->setTranslation($object, $type['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 1;
    }
}