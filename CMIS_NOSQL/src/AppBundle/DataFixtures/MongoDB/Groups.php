<?php
/**
 * @file
 * DataFixture Groups.
 */
namespace AppBundle\DataFixtures\MongoDB;

use AppBundle\DataFixtures\Fixtures\MockGroup;
use AppBundle\Document\Group;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class Groups implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Non-PHPdoc
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_group = new MockGroup();
        $groups = $mock_group->get();

        if (! empty($groups)) {
            foreach ($groups as $group) {
                $g = new Group($group['name'], $group['roles']);
                $g->setCode($group['code']);
                $manager->persist($g);
            }
            $manager->flush();
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 3;
    }
}