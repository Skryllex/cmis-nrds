<?php
/**
 * @file
 * DataFixture Genre.
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Fixtures\MockGenre;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Genres implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_genre = new MockGenre();
        $genres = $mock_genre->get();

        if (! empty($genres)) {
            $data = array();
            foreach ($genres as $genre) {
                $builder = $this->container->get(Service::BuilderGenreBuilder);
                $object = $builder->build($genre['code'], $genre['title'], $genre['description']);
                if (! empty($genre['translation']) && method_exists($builder, 'setTranslation')) {
                    $object = $builder->setTranslation($object, $genre['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 2;
    }
}