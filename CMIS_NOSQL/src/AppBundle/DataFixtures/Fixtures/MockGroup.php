<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\Group as EnumGroup;
use AppBundle\Enum\MasterTableCode;

final class MockGroup implements MockInterface
{

    public function get()
    {
        return array(
            array(
                'code' => MasterTableCode::Group . 'SP',
                'name' => 'Super administrator',
                'description' => "",
                'roles' => EnumGroup::SuperAdministrator
            ),
            array(
                'code' => MasterTableCode::Group . 'AD',
                'name' => 'Administrator',
                'description' => "",
                'roles' => EnumGroup::Administrator
            ),
            array(
                'code' => MasterTableCode::Group . 'AUTH',
                'name' => 'Authenticated',
                'description' => "",
                'roles' => EnumGroup::Authenticated
            )
        );
    }
}