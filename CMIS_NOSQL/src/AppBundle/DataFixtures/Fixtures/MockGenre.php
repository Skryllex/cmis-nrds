<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\Genre as EnumGenre;

final class MockGenre implements MockInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\DataFixtures\Fixtures\MockInterface::get()
     */
    public function get()
    {
        return array(
            array(
                'code' => EnumGenre::Action,
                'title' => 'Action',
                'description' => "Action film is a film genre in which one or more heroes are thrust into a series of challenges that typically include physical feats," . " extended fight scenes, violence, and frantic chases. Action films tend to feature a resourceful character struggling against incredible odds," . " which include life-threatening situations, a villain, or a pursuit which generally concludes in victory for the hero.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Acción',
                        'description' => "Película de acción es un género cinematográfico en el que uno o más héroes son empujados a una serie de retos a los que típicamente incluyen hazañas físicas" . "Escenas extendidas lucha, violencia y persecuciones frenéticas. Las películas de acción tienden a aparecer un personaje ingenioso lucha contra probabilidades increíbles" . "que incluyen situaciones que amenazan la vida, un villano, o una persecución que generalmente concluye con la victoria del héroe."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Adventure,
                'title' => 'Adventure',
                'description' => "Adventure films are a genre of film. Unlike action films," . " they often use their action scenes preferably to display and explore exotic locations in an energetic way." . " The subgenres of adventure films include, swashbuckler film, disaster films, and historical dramas - which is similar to the epic film genre." . " Main plot elements include quests for lost continents, a jungle, mountain, island, urban and/or desert settings," . " characters going on a treasure hunts and heroic journeys for the unknown. Adventure films are mostly set in a period background and" . " may include adapted stories of historical or fictional adventure heroes within the historical context." . " Kings, battles, rebellion or piracy are commonly seen in adventure films." . " Adventure films may also be combined with other movie genres such as, science fiction, fantasy and sometimes war films.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Aventuras',
                        'description' => "Las películas de aventura son un género de la película. A diferencia de las películas de acción" . "A menudo utilizan sus escenas de acción de preferencia para visualizar y explorar lugares exóticos de manera enérgica" . "Los subgéneros de las películas de aventuras incluyen, película espadachín, las películas de desastres, y dramas históricos - que es similar a la del género de la película épica" . "Elementos de la trama principales incluyen misiones de continentes perdidos, una selva, montaña, isla, o los entornos urbanos y / desierto," . "Caracteres pasando unos cazas del tesoro y los viajes heroicos de lo desconocido. Películas de aventura se fijan sobre todo en un período de fondo y" . "Puede incluir historias adaptadas de héroes de aventuras históricas o ficticias dentro del contexto histórico." . "Reyes, batallas, rebelión o la piratería son vistos comúnmente en películas de aventuras." . "Las películas de aventura también se pueden combinar con otros géneros de películas tales como, ciencia ficción, fantasía y, a veces las películas de guerra."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Animation,
                'title' => 'Animation',
                'description' => "Animation is the process of creating the illusion of motion and shape change by means of the rapid display of a sequence of static images" . " that minimally differ from each other. The illusion—as in motion pictures in general—is thought to rely on the phi phenomenon." . " Animators are artists who specialize in the creation of animation. Animations can be recorded on either analogue media," . " such as a flip book, motion picture film, video tape, or on digital media, including formats such as animated GIF," . " Flash animation or digital video. To display animation, a digital camera, computer, or projector are used along with new technologies" . " that are produced. Animation creation methods include the traditional animation creation method and those involving stop motion animation" . " of two and three-dimensional objects, such as paper cutouts, puppets and clay figures." . " Images are displayed in a rapid succession, usually 24, 25, 30, or 60 frames per second.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Animación',
                        'description' => "La animación es el proceso de crear la ilusión de movimiento y la forma de cambio por medio de la rápida visualización de una secuencia de imágenes estáticas" . "Que mínimamente se diferencian unos de otros. La ilusión, como en el cine en general, se cree que se basan en el fenómeno phi" . "Los animadores son artistas que se especializan en la creación de animación. Las animaciones se pueden grabar en cualquiera de los medios analógicos" . "Como un libro animado, película cinematográfica, cinta de vídeo, o en medios digitales, incluyendo formatos como GIF animado" . "Animación Flash o vídeo digital. Para visualizar la animación, una cámara digital, ordenador, proyector o se utilizan junto con las nuevas tecnologías" . "Que se producen. Métodos de creación de animación incluyen el método tradicional de la creación de la animación y los relacionados con la animación stop motion" . "De dos y tres dimensiones los objetos, tales como recortes de papel, marionetas y figuras de barro." . "Las imágenes se muestran en una rápida sucesión, por lo general 24, 25, 30, o 60 cuadros por segundo."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Comedy,
                'title' => 'Comedy',
                'description' => "Television comedy had a presence from the earliest days of broadcasting." . " Among the earliest BBC television programmes in the 1930s was Starlight, which offered a series of guests from the music hall era — singers" . " and comedians amongst them. Similarly, many early United States television programmes were variety shows including the Texaco Star Theater" . " featuring Milton Berle; comedy acts often taken from vaudeville were staples of such shows.",
                'translate' => array(
                    'es' => array(
                        'title' => 'Comedia',
                        'description' => "La televisión de la comedia tenía una presencia desde los primeros días de la radiodifusión." . "Entre los primeros programas de televisión de la BBC en la década de 1930 era Starlight, que ofrecía una serie de invitados de la era music hall - cantantes" . "Y comediantes entre ellos. Del mismo modo, muchos programas para la primera televisión de los Estados Unidos fueron los espectáculos de variedades como la Texaco Star Theater" . "Con Milton Berle, comedia actúa a menudo tomada de vodevil eran grapas de tales espectáculos."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Drama,
                'title' => 'Drama',
                'description' => "Dramatic programming in the UK, or television drama and television drama series in the United States or teledrama in Sri Lanka," . " is television program content that is scripted and (normally) fictional along the lines of a traditional drama." . " This excludes, for example, sports television, television news, reality show and game shows, stand-up comedy and variety shows." . " By convention, the term is not generally used for situation comedy or soap opera.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Drama',
                        'description' => "Programación dramática en el Reino Unido, o el drama de la televisión y la serie dramática de televisión en los Estados Unidos o teledrama en Sri Lanka" . "Es el contenido del programa de televisión que se guión y (normalmente) de ficción en la línea de un drama tradicional." . "Esto excluye, por ejemplo, la televisión de deportes, noticias de la televisión, reality y programas de juegos, stand-up comedy y espectáculos de variedades." . "Por convención, el término no se utiliza generalmente para la comedia de situación o de telenovela."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Fantasy,
                'title' => 'Fantasy',
                'description' => "Fantasy television is a genre of television programming featuring elements of the fantastic, often including magic," . " supernatural forces, or exotic fantasy worlds. Fantasy television programs are often based on tales from mythology and folklore," . " or are adapted from fantasy stories in other media. The boundaries of fantasy television overlap with Science Fiction and Horror.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Fantasía',
                        'description' => "La televisión de la fantasía es un género de la programación de televisión que ofrece elementos de lo fantástico, a menudo incluyendo magia" . "Fuerzas sobrenaturales, o mundos de fantasía exóticas. Programas de televisión Fantasía menudo se basan en cuentos de la mitología y el folclore" . "O se adaptan de las historias de fantasía en otros medios de comunicación. Los límites de la fantasía solapamiento televisión con Ciencia Ficción y Terror."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Terror,
                'title' => 'Horror/Terror',
                'description' => "Horror is a film genre seeking to elicit a negative emotional reaction from viewers by playing on the audience's primal fears." . " Inspired by literature from authors like Edgar Allan Poe, Bram Stoker, Mary Shelley, horror films have for more than a century featured" . " scenes that startle the viewer. The macabre and the supernatural are frequent themes. Thus they may overlap with the fantasy, supernatural," . " and thriller genres.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Horror/Terror',
                        'description' => "El terror es un género cinematográfico que busca provocar una reacción emocional negativa de los espectadores al jugar con los temores primordiales de la audiencia." . "Inspirado por la literatura de autores como Edgar Allan Poe, Bram Stoker, Mary Shelley, películas de terror tienen desde hace más de un siglo las funciones" . "Escenas que asustar al espectador. El macabro y lo sobrenatural son temas frecuentes. Así que pueden solaparse con la fantasía, sobrenatural y géneros de suspenso."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Reality,
                'title' => 'Reality',
                'description' => "Reality television is a genre of television programming that documents unscripted real-life situations," . " and often features an otherwise unknown cast. It differs from documentary television in that the focus tends to be on drama and personal" . " conflict, rather than simply educating viewers. Reality TV programs also often bring participants into situations and environments that" . " they would otherwise never be a part of. The genre has various standard tropes, including confessionals used by cast members to express" . " their thoughts, which often double as the shows' narration. In competition-based reality shows, a notable subset," . " there are other common elements such as one participant being eliminated per episode, a panel of judges," . " and the concept of immunity from elimination.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Reality',
                        'description' => "Televisión de la realidad es un género de la programación televisiva que documenta situaciones de la vida real sin guión" . "Ya menudo cuenta con un elenco de otro modo desconocido. Se diferencia de la televisión documental en el que el enfoque tiende a estar en el teatro y personal" . "Conflicto, en lugar de los espectadores simplemente educando. Programas de televisión de realidad también suelen llevar a los participantes en situaciones y ambientes que" . " de otro modo no ser parte de. El género tiene varios tropos estándar, incluyendo confesionarios utilizados por los miembros del reparto de expresar" . "Sus pensamientos, que a menudo sirven como narración los espectáculos '. En los reality shows basados en la competencia, un subconjunto notable" . "Hay otros elementos comunes, como uno de los participantes que se elimina por episodio, un panel de jueces" . "y el concepto de inmunidad de la eliminación."
                    )
                )
            ),
            array(
                'code' => EnumGenre::ScienceFiction,
                'title' => 'Science fiction',
                'description' => "Science fiction is a genre of fiction dealing with imaginative content such as futuristic settings," . " futuristic science and technology, space travel, time travel, faster than light travel, parallel universes and extraterrestrial life." . " It usually eschews the supernatural, and unlike the related genre of fantasy, its imaginary elements are largely plausible within" . " the scientifically established context of the story. Science fiction often explores the potential consequences of scientific" . " and other innovations, and has been called a literature of ideas.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Ciencia ficción',
                        'description' => "La ciencia ficción es un género de la ficción que trata de contenido imaginativo como la configuración futuristas" . "Futurista de ciencia y tecnología, los viajes espaciales, viajes en el tiempo, más rápido que la luz viaje, universos paralelos y la vida extraterrestre." . "Por lo general, evita lo sobrenatural, ya diferencia de género relacionado de fantasía, sus elementos imaginarios son en gran medida plausibles dentro" . "El contexto establecido científicamente de la historia. La ciencia ficción a menudo explora las consecuencias potenciales de científico" . "y otras innovaciones, y que se ha denominado una literatura de ideas."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Thriller,
                'title' => 'Thriller',
                'description' => "Thriller is a genre of literature, film, videogame stories and television programming that uses suspense, tension," . " and excitement as its main elements. Thrillers heavily stimulate the viewer's moods, giving them a high level of anticipation," . " ultra-heightened expectation, uncertainty, surprise, anxiety and terror. Films of this genre tend to be adrenaline-rushing," . " gritty, rousing and fast-paced.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Suspense',
                        'description' => "Thriller es un género de la literatura, el cine, las historias de videojuegos y programas de televisión que utiliza suspenso, tensión" . "Y la emoción como sus principales elementos. Suspense muy estimulan los estados de ánimo del espectador, dándoles un alto nivel de anticipación," . "Ultra-elevada expectativa, la incertidumbre, la sorpresa, la ansiedad y el terror. Películas de este género tienden a ser de adrenalina corriendo " . "valiente, entusiasta y rápido."
                    )
                )
            ),
            array(
                'code' => EnumGenre::Western,
                'title' => 'Western',
                'description' => "The American Film Institute defines western films as those set in the American West that embod[y] the spirit," . " the struggle and the demise of the new frontier. The term Western, used to describe a narrative film genre," . " appears to have originated with a July 1912 article in Motion Picture World Magazine." . " Most of the characteristics of Western films were part of 19th century popular Western fiction and were firmly in place" . " before film became a popular art form. Western films commonly feature protagonists such as cowboys, gunslingers, and bounty hunters," . " and are often depicted as semi-nomadic wanderers who wear Stetson hats, bandannas, spurs, and buckskins, use revolvers or rifles" . " as everyday tools of survival, and ride between dusty towns and cattle ranches on trusty steeds.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Western',
                        'description' => "El American Film Institute define películas occidentales como los establecidos en el oeste americano que embod [y] el espíritu" . "La lucha y la desaparición de la nueva frontera. El término occidental, que se utiliza para describir un género de la película narrativa" . "Parece que se originó con un artículo de julio de 1912 en Motion Picture World Magazine." . "La mayoría de las características de las películas occidentales eran parte de la ficción popular occidental del siglo 19 y estaban firmemente en su lugar" . "Antes de la película se convirtió en una forma de arte popular. Películas occidentales comúnmente cuentan con protagonistas como los vaqueros, pistoleros, y cazadores de recompensas" . "Y son a menudo representado como vagabundos seminómadas que usan sombreros Stetson, pañuelos, espuelas, y pantalones de ante, usan revólveres o rifles" . "Herramientas como cotidianas de supervivencia y paseo entre pueblos polvorientos ranchos de ganado en corceles de confianza."
                    )
                )
            )
        );
    }
}