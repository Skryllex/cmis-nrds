<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Document\User;
use AppBundle\Enum\Gender;
use AppBundle\Enum\Role;
use AppBundle\Enum\StateUser;
use AppBundle\Enum\MasterTableCode;

final class MockUser implements MockInterface
{

    public function get()
    {
        return array(
            array(
                'className' => User::class,
                'bundle' => User::getBundle(),
                'username' => 'sylar.sykes',
                'email' => "sylar.sykes@gmail.com",
                'password' => '816PT#TP618',
                'firstname' => 'Juan',
                'lastname' => 'González Fernández',
                'birth_date' => '1987-06-29',
                'gender' => Gender::Male,
                'address' => 'Avda. de Arteixo, 118',
                'postcode' => '15007',
                'city' => 'A Coruña',
                'country' => 'ES',
                'website' => '',
                'enabled' => StateUser::Enabled,
                'roles' => array(
                    Role::SuperAdministrator
                ),
                'groups' => array(
                    0 => array(
                        'code' => MasterTableCode::Group . 'SP'
                    )
                )
            )
        );
    }
}