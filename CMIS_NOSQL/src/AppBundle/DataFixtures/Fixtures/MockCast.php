<?php
/**
 * @file
 * Cast.
 */
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\TypeCast;

final class MockCast implements MockInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\DataFixtures\Fixtures\MockInterface::get()
     */
    public function get()
    {
        return array(
            0 => array(
                'name' => 'Geoffrey',
                'surname' => 'Sax',
                'biography' => "Geoffrey Sax (sometimes credited as Geoff Sax) is a British film and television director, who has worked on a variety of drama productions in both the UK and the United States.",
                'translation' => array(
                    'es' => array(
                        'biography' => "Geoffrey Sax (a veces acreditado como Geoff Sax) es un director de cine y televisión británica, que ha trabajado en una variedad de producciones de teatro en el Reino Unido y los Estados Unidos."
                    )
                ),
                'birth_date' => NULL,
                'finished_date' => NULL,
                'types_cast' => array(
                    0 => array(
                        'code' => TypeCast::Director
                    ),
                    1 => array(
                        'code' => TypeCast::ActorActress
                    )
                ),
                'characters' => array()
            ),
            1 => array(
                'name' => 'Matthew',
                'surname' => 'Jacobs',
                'biography' => "Matthew Jacobs (born July 1, 1956) is a British writer, director, producer and actor. He is known best for his extensive career writing for television shows like Doctor Who and The Young Indiana Jones Chronicles. He also directed two prize-winning TV movies for BBC films Hallelujah Anyhow (1992) and Mothertime (1998). As an actor, he stars alongside Danny Huston in Boxing Day, Bernard Rose's forthcoming adaptation of Leo Tolstoy's novella Master and Man.",
                'translation' => array(
                    'es' => array(
                        'biography' => "Matthew Jacobs (nacido el 1 de julio de, 1956) es un escritor británico, director, productor y actor. Él es mejor conocido por su extensa carrera escribiendo para la televisión muestra como Doctor Who y The joven Indiana Jones. También ha dirigido dos películas de televisión premiadas películas para la BBC Aleluya De todos modos (1992) y Mothertime (1998). Como actor, actúa junto a Danny Huston en el Boxing Day, próxima adaptación del Maestro novela de León Tolstoi y el Hombre de Bernard Rose."
                    )
                ),
                'birth_date' => '1956-07-01',
                'finished_date' => NULL,
                'types_cast' => array(
                    0 => array(
                        'code' => TypeCast::Screenwriter
                    ),
                    1 => array(
                        'code' => TypeCast::ActorActress
                    ),
                    2 => array(
                        'code' => TypeCast::Director
                    )
                ),
                'characters' => array()
            ),
            2 => array(
                'name' => 'Sydney Cecil',
                'surname' => 'Newman',
                'biography' => "Sydney Cecil Newman, OC (April 1, 1917 – October 30, 1997) was a Canadian film and television producer, who played a pioneering role in British television drama from the late 1950s to the late 1960s. After his return to Canada in 1970, Newman was appointed Acting Director of the Broadcast Programs Branch for the Canadian Radio and Television Commission (CRTC) and then head of the National Film Board of Canada (NFB). He also occupied senior positions at the Canadian Film Development Corporation and Canadian Broadcasting Corporation, and acted as an advisor to the Secretary of State.",
                'translation' => array(
                    'es' => array(
                        'biography' => "Sydney Cecil Newman, OC (abril 1, 1917 a octubre 30, 1997) fue un cine y televisión canadiense, que desempeñó un papel pionero en la serie de televisión británica de finales de 1950 a finales de 1960. Tras su regreso a Canadá en 1970, Newman fue nombrado Director Interino de la Oficina de Programas de difusión de la Comisión Canadiense de Radio y Televisión (CRTC) y luego jefe de la National Film Board de Canadá (NFB). También ocupó altos cargos en la Corporación de Cine de Canadá y Canadian Broadcasting Corporation, y actuó como asesor de la Secretaría de Estado."
                    )
                ),
                'birth_date' => '1917-04-01',
                'finished_date' => '1997-10-30',
                'types_cast' => array(
                    0 => array(
                        'code' => TypeCast::Screenwriter
                    ),
                    1 => array(
                        'code' => TypeCast::ActorActress
                    ),
                    2 => array(
                        'code' => TypeCast::Director
                    )
                ),
                'characters' => array()
            ),
            3 => array(
                'name' => 'Paul John',
                'surname' => 'McGann',
                'biography' => 'Paul John McGann (born 14 November 1959) is an English actor. He came to prominence for portraying Percy Toplis in the 1986 television serial The Monocled Mutineer. He later starred in the 1987 dark comedy Withnail and I, and as the eighth incarnation of the Doctor in the 1996 Doctor Who television film, a role he reprised in more than 70 audio dramas and the 2013 mini-episode "The Night of the Doctor".',
                'translation' => array(
                    'es' => array(
                        'biography' => 'Paul John McGann (born 14 November 1959) is an English actor. He came to prominence for portraying Percy Toplis in the 1986 television serial The Monocled Mutineer. He later starred in the 1987 dark comedy Withnail and I, and as the eighth incarnation of the Doctor in the 1996 Doctor Who television film, a role he reprised in more than 70 audio dramas and the 2013 mini-episode "The Night of the Doctor".'
                    )
                ),
                'birth_date' => '1959-11-14',
                'finished_date' => NULL,
                'types_cast' => array(
                    0 => array(
                        'code' => TypeCast::ActorActress
                    )
                ),
                'characters' => array(
                    0 => array(
                        'name' => 'The Doctor',
                        'surname' => NULL,
                        'biography' => NULL,
                    )
                )
            ),
            4 => array(
                'name' => 'Eric Anthony',
                'surname' => 'Roberts',
                'biography' => 'Eric Anthony Roberts (Biloxi, Mississippi, April 18, 1956) is an American actor of film, television and theater.',
                'translation' => array(
                    'es' => array(
                        'biography' => 'Eric Anthony Roberts (Biloxi, Misisipi, 18 de abril de 1956) es un actor estadounidense de cine, televisión y teatro.'
                    )
                ),
                'birth_date' => '1959-11-14',
                'finished_date' => NULL,
                'types_cast' => array(
                    0 => array(
                        'code' => TypeCast::ActorActress
                    ),
                    1 => array(
                        'code' => TypeCast::Producer
                    )
                ),
                'characters' => array(
                    0 => array(
                        'name' => 'The Master',
                        'surname' => NULL,
                        'biography' => NULL,
                    ),
                    1 => array(
                        'name' => 'Bruce',
                        'surname' => NULL,
                        'biography' => NULL,
                    ),
                )
            ),
            5 => array(
                'name' => 'Daphne Lee',
                'surname' => 'Ashbrook',
                'biography' => "Daphne Lee Ashbrook (born January 30, 1963) is an American actress.",
                'translation' => array(
                    'es' => array(
                        'biography' => "Daphne Ashbrook Lee (nacido el 30 de enero 1963) es una actriz estadounidense."
                    )
                ),
                'birth_date' => '1963-01-30',
                'finished_date' => NULL,
                'types_cast' => array(
                    0 => array(
                        'code' => TypeCast::ActorActress
                    )
                ),
                'characters' => array(
                    0 => array(
                        'name' => 'Grace',
                        'surname' => 'Holloway',
                        'biography' => "Dr. Grace Holloway is a fictional character played by Daphne Ashbrook in the 1996 television movie Doctor Who, a continuation of the long-running British science fiction television series Doctor Who. A cardiologist from San Francisco in 1999, she assists the Eighth Doctor in defeating the renegade Time Lord, the Master.",
                        'translation' => array(
                            'es' => array(
                                'biography' => "Doctora Grace Holloway es un personaje de ficción interpretado por Daphne Ashbrook en el doctor película para televisión 1996 Quién, una continuación del doctor de larga duración británico de ciencia ficción serie de televisión ¿Quién. Un cardiólogo de San Francisco en 1999, ella asiste al Octavo Doctor en derrotar al renegado señor del tiempo, el Maestro."
                            )
                        )
                    )
                )
            )
        );
    }
}