<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\Genre;
use AppBundle\Enum\TypeCast;

final class MockMovie implements MockInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\DataFixtures\Fixtures\MockInterface::get()
     */
    public function get()
    {
        return array(
            array(
                'title' => 'Doctor Who: The Movie',
                'synopsis' => "Doctor Who, also referred to as Doctor Who: The Movie to distinguish it from the television series of the same name, is a British-American-Canadian television film continuing the British science fiction television series Doctor Who. Developed as a co-production between BBC Worldwide, Universal Studios, 20th Century Fox and the American network Fox, the 1996 television film premiered on 12 May 1996 on CITV in Edmonton, Alberta, Canada (which was owned by WIC at the time before being acquired by Canwest Global in 2000), 15 days before its first showing in the United Kingdom on BBC One and two days before being broadcast in the United States on Fox. It was also shown in some countries for a limited time in cinemas.",
                'translation' => array(
                    'es' => array(
                        'synopsis' => "El doctor Who, también conocido como doctor Who: The Movie para distinguirla de la serie de televisión del mismo nombre, es una película de la televisión británica-estadounidense canadiense continuar el doctor serie de televisión de ciencia ficción británica Quién. Desarrollado como una co-producción entre BBC Worldwide, Universal Studios, 20th Century Fox y la cadena estadounidense Fox, la película de televisión 1.996 se estrenó el 12 de mayo de 1996 sobre CITV en Edmonton, Alberta, Canadá (que era propiedad de WIC en el momento antes de ser adquirida por Canwest Global en 2000), 15 días antes de su primera actuación en el Reino Unido en BBC One y dos días antes de ser transmitido en los Estados Unidos por Fox. También se demostró en algunos países por un tiempo limitado en los cines."
                    )
                ),
                'year' => '1996',
                'duration' => '89',
                'country' => 'Canada, United States, United Kingdom',
                'genres' => array(
                    0 => array(
                        'code' => Genre::Adventure
                    ),
                    1 => array(
                        'code' => Genre::Drama
                    ),
                    2 => array(
                        'code' => Genre::ScienceFiction
                    )
                ),
                'casts' => array(
                    0 => array(
                        'name' => 'Geoffrey',
                        'surname' => 'Sax',
                        'types_cast' => array(
                            0 => array(
                                'code' => TypeCast::Director
                            )
                        ),
                        'characters' => array()
                    ),
                    1 => array(
                        'name' => 'Matthew',
                        'surname' => 'Jacobs',
                        'types_cast' => array(
                            0 => array(
                                'code' => TypeCast::Screenwriter
                            )
                        ),
                        'characters' => array()
                    ),
                    2 => array(
                        'name' => 'Sydney Cecil',
                        'surname' => 'Newman',
                        'types_cast' => array(
                            0 => array(
                                'code' => TypeCast::Screenwriter
                            )
                        ),
                        'characters' => array()
                    ),
                    3 => array(
                        'name' => 'Paul John',
                        'surname' => 'McGann',
                        'types_cast' => array(
                            0 => array(
                                'code' => TypeCast::ActorActress
                            )
                        ),
                        'characters' => array(
                            0 => array(
                                'name' => 'The Doctor',
                            )
                        )
                    ),
                    4 => array(
                        'name' => 'Eric Anthony',
                        'surname' => 'Roberts',
                        'types_cast' => array(
                            0 => array(
                                'code' => TypeCast::ActorActress
                            )
                        ),
                        'characters' => array(
                            0 => array(
                                'name' => 'The Master',
                            ),
                            1 => array(
                                'name' => 'Bruce',
                            )
                        )
                    ),
                    5 => array(
                        'name' => 'Daphne Lee',
                        'surname' => 'Ashbrook',
                        'types_cast' => array(
                            0 => array(
                                'code' => TypeCast::ActorActress
                            )
                        ),
                        'characters' => array(
                            0 => array(
                                'name' => 'Grace',
                                'surname' => 'Holloway',
                            )
                        )
                    )
                ),
                'characters' => array(
                    0 => array(
                        'name' => 'The Doctor'
                    ),
                    1 => array(
                        'name' => 'The Master'
                    ),
                    2 => array(
                        'name' => 'Bruce',
                    ),
                    3 => array(
                        'name' => 'Grace',
                        'surname' => 'Holloway'
                    )
                )
            )
        );
    }
}