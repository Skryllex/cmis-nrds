<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Builder\ContentMediaCredit;

use SylrSyksSoftSymfony\CoreBundle\Builder\BuilderInterface;

interface ContentMediaCreditBuilderInterface extends BuilderInterface
{

    /**
     * Add characters.
     *
     * @param array $characters
     */
    public function addCharacters(array $characters);

    /**
     * Add typeCast.
     *
     * @param array $typesCast
     */
    public function addTypesCast(array $typesCast);

    /**
     * Create a document.
     *
     * @param string $name
     *            Name.
     * @param string $surname
     *            Surname.
     * @param string $biography
     *            Biography.
     * @param \DateTime $birthDate
     *            BirthDate.
     */
    public function create($name, $surname = NULL, $biography = NULL, \DateTime $birthDate = NULL, \DateTime $finishedDate = NULL);

}