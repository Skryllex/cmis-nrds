<?php
namespace AppBundle\Builder\ContentMediaCredit;

use AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderInterface;
use SylrSyksSoftSymfony\CoreBundle\Builder\Document\DocumentBuilder;

final class CharacterBuilder extends DocumentBuilder implements ContentMediaCreditBuilderInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderInterface::addCharacters()
     */
    public function addCharacters(array $characters)
    {
        return FALSE;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderInterface::addTypesCast()
     */
    public function addTypesCast(array $typesCast)
    {
        return FALSE;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderInterface::create()
     */
    public function create($name, $surname = NULL, $biography = NULL, \DateTime $birthDate = NULL, \DateTime $finishedDate = NULL)
    {
        $this->object->setName($name)
            ->setSurname($surname)
            ->setBiography($biography);
    }
}