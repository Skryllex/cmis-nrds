<?php
namespace AppBundle\Builder\ContentMediaCredit;

use AppBundle\Document\Cast;
use AppBundle\Document\Character;
use AppBundle\Enum\Service;

trait ContentMediaCreditBuilderTrait
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMedia\ContentMediaBuilderInterface::addCasting()
     */
    public function addCasting(array $casts)
    {
        if (! empty($casts)) {
            foreach ($casts as $cast) {
                $conditions = array(
                    'name' => $cast['name']
                );

                if (isset($cast['surname'])) {
                    $conditions['surname'] = $cast['surname'];
                }

                $c = $this->manager->getRepository(Cast::getBundle())->findOneBy($conditions);

                if (! isset($c)) {
                    $builder = $this->container->get(Service::BuilderContentMediaCreditCastBuilder);
                    $c = $builder->build($cast['name'], $cast['surname'], $cast['biography'], $cast['birth_date'], $cast['finished_date'], $cast['types_cast'], $cast['characters']);
                    if (! empty($cast['translation']) && method_exists($builder, 'setTranslation')) {
                        $c = $builder->setTranslation($c, $cast['translation']);
                    }
                } elseif (isset($c) && ! empty($cast['types_cast'])) {
                    $builder = $this->container->get(Service::BuilderContentMediaCreditCastBuilder);
                    $bDate = ($c->getBirthDate() != NULL) ? $c->getBirthDate()->format("Y-m-d") : NULL;
                    $fDate = ($c->getFinishedDate() != NULL) ? $c->getFinishedDate()->format("Y-m-d") : NULL;
                    $ch = (isset($cast['characters'])) ? $cast['characters'] : array();
                    $c = $builder->build($c->getName(), $c->getSurname(), $c->getBiography(), $bDate, $fDate, $cast['types_cast'], $ch);
                }

                $this->object->addCast($c);
            }
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderInterface::addCharacters()
     */
    public function addCharacters(array $characters)
    {
        if (! empty($characters)) {
            foreach ($characters as $character) {
                $conditions = array(
                    'name' => $character['name']
                );

                if (isset($character['surname'])) {
                    $conditions['surname'] = $character['surname'];
                }

                $ch = $this->manager->getRepository(Character::getBundle())->findOneBy($conditions);

                if (! isset($ch)) {
                    $builder = $this->container->get(Service::BuilderContentMediaCreditCharacterBuilder);
                    $ch = $builder->build($character['name'], $character['surname'], $character['biography']);
                    if (! empty($character['translation']) && method_exists($builder, 'setTranslation')) {
                        $ch = $builder->setTranslation($ch, $character['translation']);
                    }
                }
                $this->object->addCharacter($ch);
            }
        }
    }
}