<?php
namespace AppBundle\Builder\ContentMediaCredit;

use AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderInterface;
use AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderTrait;
use AppBundle\Builder\MasterTable\MasterTableBuilderTrait;
use SylrSyksSoftSymfony\CoreBundle\Builder\Document\DocumentBuilder;

final class CastBuilder extends DocumentBuilder implements ContentMediaCreditBuilderInterface
{
    use ContentMediaCreditBuilderTrait;
    use MasterTableBuilderTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderInterface::create()
     */
    public function create($name, $surname = NULL, $biography = NULL, \DateTime $birthDate = NULL, \DateTime $finishedDate = NULL)
    {
        $this->object->setName($name)
            ->setSurname($surname)
            ->setBiography($biography)
            ->setBirthDate($birthDate)
            ->setFinishedDate($finishedDate);
    }
}