<?php
/**
 * @file
 * Builder content media credit.
 */
namespace AppBundle\Builder\ContentMediaCredit;

use SylrSyksSoftSymfony\CoreBundle\Builder\AbstractTranslatableBuilder;

final class ContentMediaCreditBuilder extends AbstractTranslatableBuilder
{
    /**
     * Build object.
     *
     * @param string $name
     *            Name.
     * @param string $surname
     *            Surname.
     * @param string $biography
     *            Biography.
     * @param \DateTime $birthDate
     *            BirthDate.
     * @param array $typesCast
     *            An array of types cast.
     * @param array $characters
     *            An array of characters.
     */
    public function build($name, $surname, $biography, $birthDate = NULL, $finishedDate = NULL, $typesCast = array(), $characters = array())
    {
        $bDate = (isset($birthDate)) ? \DateTime::createFromFormat("Y-m-d", $birthDate) : $birthDate;
        $fDate = (isset($finishedDate)) ? \DateTime::createFromFormat("Y-m-d", $finishedDate) : $finishedDate;

        $this->builder->create($name, $surname, $biography, $bDate, $fDate);

        if (! empty($typesCast)) {
            $this->builder->addTypesCast($typesCast);
        }

        if (! empty($characters)) {
            $this->builder->addCharacters($characters);
        }

        return $this->builder->get();
    }
}