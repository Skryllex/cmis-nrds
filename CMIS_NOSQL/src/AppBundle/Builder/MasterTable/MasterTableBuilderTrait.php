<?php
namespace AppBundle\Builder\MasterTable;

use AppBundle\Document\Genre;
use AppBundle\Document\TypeCast;
use AppBundle\Enum\Service;

trait MasterTableBuilderTrait
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderInterface::addTypesCast()
     */
    public function addTypesCast(array $typesCast)
    {
        if (! empty($typesCast)) {
            foreach ($typesCast as $typeCast) {
                $conditions = array(
                    'code' => $typeCast['code']
                );
                $type = $this->manager->getRepository(TypeCast::getBundle())->findOneBy($conditions);

                if (! isset($type)) {
                    $builder = $this->container->get(Service::BuilderTypeCastBuilder);
                    $type = $builder->build($typeCast['code'], $typeCast['title'], $typeCast['description']);
                    if (! empty($type['translation']) && method_exists($builder, 'setTranslation')) {
                        $type = $builder->setTranslation($type, $typeCast['translation']);
                    }
                }
                $this->object->addTypeCast($type);
            }
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMedia\ContentMediaBuilderInterface::addGenres()
     */
    public function addGenres(array $genres)
    {
        if (! empty($genres)) {
            foreach ($genres as $genre) {
                $conditions = array(
                    'code' => $genre['code']
                );
                $g = $this->manager->getRepository(Genre::getBundle())->findOneBy($conditions);

                if (! isset($g)) {
                    $builder = $this->container->get(Service::BuilderGenreBuilder);
                    $g = $builder->build($genre['code'], $genre['title'], $genre['description']);
                    if (! empty($genre['translation']) && method_exists($builder, 'setTranslation')) {
                        $g = $builder->setTranslation($g, $genre['translation']);
                    }
                }
                $this->object->addGenre($g);
            }
        }
    }
}