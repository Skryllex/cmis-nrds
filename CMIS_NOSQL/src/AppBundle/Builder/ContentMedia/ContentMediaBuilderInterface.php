<?php
namespace AppBundle\Builder\ContentMedia;

use SylrSyksSoftSymfony\CoreBundle\Builder\BuilderInterface;

interface ContentMediaBuilderInterface extends BuilderInterface
{
    /**
     * Create an object.
     */
    public function create($title, $synopsis, $year, $country, $yearEnd = NULL, $duration = NULL);

    /**
     * Add genres.
     *
     * @param array $genres
     */
    public function addGenres(array $genres);

    /**
     * Add casts.
     *
     * @param array $casts
     */
    public function addCasting(array $casts);

    /**
     * Add characters
     *
     * @param array $characters
     */
    public function addCharacters(array $characters);
}