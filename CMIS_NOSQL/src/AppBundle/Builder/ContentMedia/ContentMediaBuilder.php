<?php
namespace AppBundle\Builder\ContentMedia;

use SylrSyksSoftSymfony\CoreBundle\Builder\AbstractTranslatableBuilder;

final class ContentMediaBuilder extends AbstractTranslatableBuilder
{

    /**
     * Content media builder.
     *
     * @param string $title
     *            Title.
     * @param string $synopsis
     *            Synopsis.
     * @param string $year
     *            Year.
     * @param string $country
     *            Country.
     * @param string $yearEnd
     *            End year.
     * @param array $genres
     *            Genres.
     * @param array $casting
     *            Casting.
     * @param array $characters
     *            Characters.
     */
    public function build($title, $synopsis, $year, $country, $yearEnd = NULL, $duration = NULL, $genres = array(), $casting = array(), $characters = array())
    {
        $this->builder->create($title, $synopsis, $year, $country, $yearEnd, $duration);

        if (! empty($genres)) {
            $this->builder->addGenres($genres);
        }

        if (! empty($casting)) {
            $this->builder->addCasting($casting);
        }

        if (! empty($characters)) {
            $this->builder->addCharacters($characters);
        }

        return $this->builder->get();
    }
}