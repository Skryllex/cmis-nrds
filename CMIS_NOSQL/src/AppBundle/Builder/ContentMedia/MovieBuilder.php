<?php
namespace AppBundle\Builder\ContentMedia;

use AppBundle\Builder\ContentMedia\ContentMediaBuilderInterface;
use AppBundle\Builder\MasterTable\MasterTableBuilderTrait;
use AppBundle\Builder\ContentMediaCredit\ContentMediaCreditBuilderTrait;
use SylrSyksSoftSymfony\CoreBundle\Builder\Document\DocumentBuilder;

final class MovieBuilder extends DocumentBuilder implements ContentMediaBuilderInterface
{
    use MasterTableBuilderTrait;
    use ContentMediaCreditBuilderTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\ContentMedia\ContentMediaBuilderInterface::create()
     */
    public function create($title, $synopsis, $year, $country, $yearEnd = NULL, $duration = NULL)
    {
        $this->object->setTitle($title)
            ->setSynopsis($synopsis)
            ->setYear($year)
            ->setCountry($country)
            ->setDuration($duration);
    }
}