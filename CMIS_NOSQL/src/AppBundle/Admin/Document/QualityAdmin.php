<?php
/**
 * @file
 * Manage the entity Quality.
 */
namespace AppBundle\Admin\Document;

use SylrSyksSoftSymfony\CoreBundle\Admin\AbstractMasterTableAdmin;

final class QualityAdmin extends AbstractMasterTableAdmin
{
    /**
     * Base pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = self::BASE_ROUTE_PATTERN . '/quality';
}