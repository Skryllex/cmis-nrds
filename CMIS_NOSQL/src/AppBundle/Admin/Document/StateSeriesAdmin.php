<?php
/**
 * @file
 * Manage the entity StateSeries.
 */
namespace AppBundle\Admin\Document;

use SylrSyksSoftSymfony\CoreBundle\Admin\AbstractMasterTableAdmin;

final class StateSeriesAdmin extends AbstractMasterTableAdmin
{
    /**
     * Base pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = self::BASE_ROUTE_PATTERN . '/state-series';
}