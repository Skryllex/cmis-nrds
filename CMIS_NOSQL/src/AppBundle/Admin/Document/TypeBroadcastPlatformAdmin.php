<?php
/**
 * @file
 * Manage the entity TypeBroadcastPlatform.
 */
namespace AppBundle\Admin\Document;

use SylrSyksSoftSymfony\CoreBundle\Admin\AbstractMasterTableAdmin;

final class TypeBroadcastPlatformAdmin extends AbstractMasterTableAdmin
{
    /**
     * Base pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = self::BASE_ROUTE_PATTERN . '/type-broadcast-platform';
}