<?php
/**
 * @file
 * Manage the entity TypeCast.
 */
namespace AppBundle\Admin\Document;

use SylrSyksSoftSymfony\CoreBundle\Admin\AbstractMasterTableAdmin;

final class TypeCastAdmin extends AbstractMasterTableAdmin
{

    /**
     * Base pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = self::BASE_ROUTE_PATTERN . '/type-cast';
}