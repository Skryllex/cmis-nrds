<?php
/**
 * @file
 * Manage the document Group.
 */
namespace AppBundle\Admin\Document;

use AppBundle\Enum\Role;
use AppBundle\Twig\Enum\Bootstrap\Css\Grid\MDDevices;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\UserBundle\Admin\Document\GroupAdmin as SonataGroupAdmin;
use Symfony\Component\Validator\Constraints as Assert;

final class GroupAdmin extends SonataGroupAdmin
{

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\AdminBundle\Admin\Admin::configureListFields($list)
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')->add('roles');

        if ($this->isGranted(Role::SuperAdministrator)) {
            $listMapper->add('_action', 'actions', array(
                'label' => 'Actions',
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array()
                )
            ));
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\UserBundle\Admin\Model\UserAdmin::configureShowFields()
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->with('General', array(
            'class' => MDDevices::SixColumns
        ))
            ->add('code', NULL, array(
            'label' => 'Code'
        ))
            ->add('name', NULL, array(
            'label' => 'Name'
        ))
            ->end()
            ->with('Roles', array(
            'class' => MDDevices::TwelveColumns
        ))
            ->add('roles', NULL, array(
            'label' => 'Roles'
        ))
            ->end();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\UserBundle\Admin\Model\GroupAdmin::configureFormFields()
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->tab('Group')
            ->with('General', array(
            'class' => MDDevices::SixColumns
        ))
            ->add('code', 'text', array(
            'label' => 'Code',
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Length(array(
                    'min' => 3,
                    'max' => 50
                ))
            )
        ))
            ->add('name')
            ->end()
            ->end()
            ->tab('Security')
            ->with('Roles', array(
            'class' => MDDevices::TwelveColumns
        ))
            ->add('roles', 'sonata_security_roles', array(
            'expanded' => TRUE,
            'multiple' => TRUE,
            'required' => FALSE
        ))
            ->end()
            ->end();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\AdminBundle\Admin\Admin::validate($errorElement, $object)
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $exists_code = $this->modelManager->findOneBy($this->getClass(), array(
            'code' => $object->getCode()
        ));

        if (isset($exists_code)) {
            $errorElement->with('code')
                ->addViolation('The code value already exist.')
                ->end();
        }

        $exists_title = $this->modelManager->findOneBy($this->getClass(), array(
            'name' => $object->getName()
        ));

        if (isset($exists_title)) {
            $errorElement->with('name')
                ->addViolation('The name value already exist.')
                ->end();
        }
    }
}