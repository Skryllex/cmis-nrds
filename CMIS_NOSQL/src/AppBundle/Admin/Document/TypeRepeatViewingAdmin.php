<?php
/**
 * @file
 * Manage the entity TypeRepeatViewing.
 */
namespace AppBundle\Admin\Document;

use SylrSyksSoftSymfony\CoreBundle\Admin\AbstractMasterTableAdmin;

final class TypeRepeatViewingAdmin extends AbstractMasterTableAdmin
{

    /**
     * Base pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = self::BASE_ROUTE_PATTERN . '/type-repeat-viewing';
}