<?php
/**
 * @file
 * Movie controller.
 */
namespace AppBundle\Controller;

use AppBundle\Document\Movie;
use AppBundle\Enum\TypeCast;
use Hostnet\Component\Form\Simple\SimpleFormProvider;
use JMS\DiExtraBundle\Annotation as JMSDiExtra;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as SensioFrExtra;
use SylrSyksSoftSymfony\CoreBundle\Controller\Document\DocumentServiceController;
use SylrSyksSoftSymfony\CoreBundle\Document\DocumentManager;
use SylrSyksSoftSymfony\CoreBundle\Document\Repository\DocumentRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * @SensioFrExtra\Route(service="cmis_nosql.controller.movie")
 */
final class MovieController extends DocumentServiceController
{

    /**
     *
     * @param DocumentManager $manager
     * @param DocumentRepository $repository
     *
     * @JMSDiExtra\InjectParams({
     *      "manager" = @JMSDiExtra\Inject("cmis_nosql.manager.movie"),
     *      "repository" = @JMSDiExtra\Inject("cmis_nosql.repository.movie")
     * })
     */
    public function __construct(SimpleFormProvider $formProvider, $bundle_name, $class_name, DocumentManager $manager, DocumentRepository $repository)
    {
        parent::__construct($formProvider, $bundle_name, $class_name, $manager, $repository);
    }

    /**
     * Get all movies.
     *
     * @SensioFrExtra\Route(name="cmis_nosql_movies", path="/movies")
     * @SensioFrExtra\Method(methods={"GET"})
     * @SensioFrExtra\Template(template="AppBundle:Movie:search.html.twig", vars={"movies"})
     */
    public function moviesAction()
    {
        $criteria = array(
            'deletedAt' => NULL
        );
        
        $sort = array(
            'updatedAt' => 'DESC'
        );

        $movies = $this->repository->findMovies($criteria, $sort);

        if (! $movies) {
            throw $this->createNotFoundException('There are no elements.');
        }

        return array(
            'movies' => $movies
        );
    }

    /**
     * View movie.
     *
     * @param int $year
     *            Year.
     * @param string $name
     *            Name.
     *
     * @SensioFrExtra\Route(name="cmis_nosql_movies_view", path="/movies/show/{slug}", options={"slug"=".+"})
     * @SensioFrExtra\ParamConverter(name="movie", class="AppBundle:Movie", options={"mapping":{"slug"="slug"}})
     * @SensioFrExtra\Method(methods={"GET"})
     * @SensioFrExtra\Template(template="AppBundle:Movie:view.html.twig", vars={"movie", "directors", "writers", "casts", "producers", "broadcast_platforms"})
     */
    public function viewAction(Movie $movie)
    {

        $casts = $movie->getCredits();
        $directors = $movie->getCredits(TypeCast::Director);
        $writers = $movie->getCredits(TypeCast::Screenwriter);
        $producers = $movie->getCredits(TypeCast::Producer);
        $broadcast_platforms = $movie->getBroadcastPlatforms();

        return array(
            'movie' => $movie,
            'directors' => $directors,
            'writers' => $writers,
            'casts' => $casts,
            'producers' => $producers,
            'broadcast_platforms' => $broadcast_platforms->toArray()
        );
    }

    /**
     * View movie.
     *
     * @param int $year
     *            Year.
     * @param string $name
     *            Name.
     *
     * @SensioFrExtra\Route(name="cmis_nosql_movies_credits", path="/movies/{id}/credits", options={"id"=".+"})
     * @SensioFrExtra\ParamConverter(name="movie", class="AppBundle:Movie", options={"mapping":{"id"="id"}})
     * @SensioFrExtra\Method(methods={"GET"})
     * @SensioFrExtra\Template(template="AppBundle:Movie:view.html.twig", vars={"movie", "directors", "writers", "casts", "producers"})
     */
    public function viewCreditsAction(Movie $movie)
    {
        $casts = $movie->getCredits();
        $directors = $movie->getCredits(TypeCast::Director);
        $writers = $movie->getCredits(TypeCast::Screenwriter);
        $producers = $movie->getCredits(TypeCast::Producer);

        return array(
            'movie' => $movie,
            'directors' => $directors,
            'writers' => $writers,
            'casts' => $casts,
            'producers' => $producers
        );
    }

    /**
     * Create movie.
     *
     * @SensioFrExtra\Route(name="cmis_nosql_movies_create", path="/movies/add")
     * @SensioFrExtra\Method(methods={"GET", "POST"})
     * @SensioFrExtra\Template(template="AppBundle:Movie:action.html.twig", vars={"form", "action"})
     */
    public function createAction(Request $request)
    {
        return $this->createCreateForm($request, "POST", "cmis_nosql_movies_create");
    }

    /**
     * Edit movie.
     *
     * @SensioFrExtra\Route(name="cmis_nosql_movies_edit", path="/movies/{id}/edit", options={"id"=".+"})
     * @SensioFrExtra\ParamConverter(name="movie", class="AppBundle:Movie", options={"mapping":{"id"="id"}})
     * @SensioFrExtra\Method(methods={"GET", "POST"})
     * @SensioFrExtra\Template(template="AppBundle:Movie:action.html.twig", vars={"form", "action"})
     */
    public function editAction(Movie $movie, Request $request)
    {
        return $this->createUpdateForm($request, $movie, "POST", "cmis_nosql_movies_edit");
    }
    
    /**
     * Edit movie.
     *
     * @SensioFrExtra\Route(name="cmis_nosql_movies_delete", path="/movies/{id}/delete", options={"id"=".+"})
     * @SensioFrExtra\ParamConverter(name="movie", class="AppBundle:Movie", options={"mapping":{"id"="id"}})
     * @SensioFrExtra\Method(methods={"GET", "POST"})
     * @SensioFrExtra\Template(template="AppBundle:Movie:action.html.twig", vars={"form", "action", "delete"})
     */
    public function removeAction(Movie $movie, Request $request)
    {
        return $this->createDeleteForm($request, $movie, "POST", "title", "cmis_nosql_movies_delete");
    }
}