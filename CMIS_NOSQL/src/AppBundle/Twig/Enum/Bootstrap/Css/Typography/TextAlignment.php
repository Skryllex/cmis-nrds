<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Typography;

use MyCLabs\Enum\Enum;

final class TextAlignment extends Enum
{
    const __default = self::Center;

    const Left = 'text-left';

    const Center = 'text-center';

    const Right = 'text-right';

    const Justify = 'text-justify';

    const Nowrap = 'text-nowrap';
}