<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Grid;

use MyCLabs\Enum\Enum;

final class MDDevices extends Enum
{
    const __default = self::TwelveColumns;

    const ResetColumns = 'col-md-offset-0';

    const OneColumn = 'col-md-offset-1';

    const TwoColumns = 'col-md-offset-2';

    const ThreeColumns = 'col-md-offset-3';

    const FourColumns = 'col-md-offset-4';

    const FiveColumns = 'col-md-offset-5';

    const SixColumns = 'col-md-offset-6';

    const SevenColumns = 'col-md-offset-7';

    const EightColumns = 'col-md-offset-8';

    const NineColumns = 'col-md-offset-9';

    const TenColumns = 'col-md-offset-10';

    const ElevenColumns = 'col-md-offset-11';

    const TwelveColumns = 'col-md-offset-12';
}