<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Grid;

use MyCLabs\Enum\Enum;

final class XSDevices extends Enum
{
    const __default = self::TwelveColumns;

    const ResetColumns = 'col-xs-offset-0';

    const OneColumn = 'col-xs-offset-1';

    const TwoColumns = 'col-xs-offset-2';

    const ThreeColumns = 'col-xs-offset-3';

    const FourColumns = 'col-xs-offset-4';

    const FiveColumns = 'col-xs-offset-5';

    const SixColumns = 'col-xs-offset-6';

    const SevenColumns = 'col-xs-offset-7';

    const EightColumns = 'col-xs-offset-8';

    const NineColumns = 'col-xs-offset-9';

    const TenColumns = 'col-xs-offset-10';

    const ElevenColumns = 'col-xs-offset-11';

    const TwelveColumns = 'col-xs-offset-12';
}