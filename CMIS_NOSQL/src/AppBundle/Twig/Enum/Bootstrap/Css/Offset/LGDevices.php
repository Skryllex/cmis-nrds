<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Grid;

use MyCLabs\Enum\Enum;

final class LGDevices extends Enum
{
    const __default = self::TwelveColumns;

    const ResetColumns = 'col-lg-offset-0';

    const OneColumn = 'col-lg-offset-1';

    const TwoColumns = 'col-lg-offset-2';

    const ThreeColumns = 'col-lg-offset-3';

    const FourColumns = 'col-lg-offset-4';

    const FiveColumns = 'col-lg-offset-5';

    const SixColumns = 'col-lg-offset-6';

    const SevenColumns = 'col-lg-offset-7';

    const EightColumns = 'col-lg-offset-8';

    const NineColumns = 'col-lg-offset-9';

    const TenColumns = 'col-lg-offset-10';

    const ElevenColumns = 'col-lg-offset-11';

    const TwelveColumns = 'col-lg-offset-12';
}