<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Grid;

use MyCLabs\Enum\Enum;

final class SMDevices extends Enum
{
    const __default = self::TwelveColumns;

    const ResetColumns = 'col-xs-offset-0';

    const OneColumn = 'col-sm-offset-1';

    const TwoColumns = 'col-sm-offset-2';

    const ThreeColumns = 'col-sm-offset-3';

    const FourColumns = 'col-sm-offset-4';

    const FiveColumns = 'col-sm-offset-5';

    const SixColumns = 'col-sm-offset-6';

    const SevenColumns = 'col-sm-offset-7';

    const EightColumns = 'col-sm-offset-8';

    const NineColumns = 'col-sm-offset-9';

    const TenColumns = 'col-sm-offset-10';

    const ElevenColumns = 'col-sm-offset-11';

    const TwelveColumns = 'col-sm-offset-12';
}