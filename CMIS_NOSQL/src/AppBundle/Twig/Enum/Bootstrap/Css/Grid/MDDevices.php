<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Grid;

use MyCLabs\Enum\Enum;

final class MDDevices extends Enum
{
    const __default = self::TwelveColumns;

    const OneColumn = 'col-md-1';

    const TwoColumns = 'col-md-2';

    const ThreeColumns = 'col-md-3';

    const FourColumns = 'col-md-4';

    const FiveColumns = 'col-md-5';

    const SixColumns = 'col-md-6';

    const SevenColumns = 'col-md-7';

    const EightColumns = 'col-md-8';

    const NineColumns = 'col-md-9';

    const TenColumns = 'col-md-10';

    const ElevenColumns = 'col-md-11';

    const TwelveColumns = 'col-md-12';
}