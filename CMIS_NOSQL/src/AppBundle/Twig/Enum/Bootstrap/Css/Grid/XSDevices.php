<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Grid;

use MyCLabs\Enum\Enum;

final class XSDevices extends Enum
{
    const __default = self::TwelveColumns;

    const OneColumn = 'col-xs-1';

    const TwoColumns = 'col-xs-2';

    const ThreeColumns = 'col-xs-3';

    const FourColumns = 'col-xs-4';

    const FiveColumns = 'col-xs-5';

    const SixColumns = 'col-xs-6';

    const SevenColumns = 'col-xs-7';

    const EightColumns = 'col-xs-8';

    const NineColumns = 'col-xs-9';

    const TenColumns = 'col-xs-10';

    const ElevenColumns = 'col-xs-11';

    const TwelveColumns = 'col-xs-12';
}