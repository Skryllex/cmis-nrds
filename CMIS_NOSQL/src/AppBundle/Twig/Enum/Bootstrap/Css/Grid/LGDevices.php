<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Grid;

use MyCLabs\Enum\Enum;

final class LGDevices extends Enum
{
    const __default = self::TwelveColumns;

    const OneColumn = 'col-lg-1';

    const TwoColumns = 'col-lg-2';

    const ThreeColumns = 'col-lg-3';

    const FourColumns = 'col-lg-4';

    const FiveColumns = 'col-lg-5';

    const SixColumns = 'col-lg-6';

    const SevenColumns = 'col-lg-7';

    const EightColumns = 'col-lg-8';

    const NineColumns = 'col-lg-9';

    const TenColumns = 'col-lg-10';

    const ElevenColumns = 'col-lg-11';

    const TwelveColumns = 'col-lg-12';
}