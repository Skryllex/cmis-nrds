<?php
namespace AppBundle\Twig\Enum\Bootstrap\Css\Grid;

use MyCLabs\Enum\Enum;

final class SMDevices extends Enum
{
    const __default = self::TwelveColumns;

    const OneColumn = 'col-sm-1';

    const TwoColumns = 'col-sm-2';

    const ThreeColumns = 'col-sm-3';

    const FourColumns = 'col-sm-4';

    const FiveColumns = 'col-sm-5';

    const SixColumns = 'col-sm-6';

    const SevenColumns = 'col-sm-7';

    const EightColumns = 'col-sm-8';

    const NineColumns = 'col-sm-9';

    const TenColumns = 'col-sm-10';

    const ElevenColumns = 'col-sm-11';

    const TwelveColumns = 'col-sm-12';
}