
!function ($) {
    "use strict";
    
    var Main = {
    	init : function () {
    		Main.setStickyElements(".sticky-form-actions");
    	},
    	setStickyElements : function (selector) {
    		var wrapper = $(document).find(".content-wrapper");
    		var block_wrapper = $(document).find(".sticky-wrapper.waypoint");
    		
    		var bottom = $(selector + ".bottom");
    		
    		if (block_wrapper.length) {
    			var data_css_block = $(block_wrapper).attr('data-css-block');
    			var cms_block = $(block_wrapper).find('.cms-block');
    			cms_block.addClass(data_css_block);
    			new Waypoint.Sticky({
                    element: $(cms_block)
                });
    		}
    		
    		if (bottom.length) {
                new Waypoint({
                    element: $(wrapper),
                    offset: 'bottom-in-view',
                    handler: function(direction) {
                        var position = $('.form-section.form-wrapper .form-content').outerHeight() + $(bottom).outerHeight() - 2;
                        
                        if (position < $(bottom).offset().top) {
                            $(bottom).removeClass('stuck');
                        }

                        if (direction == 'up') {
                            $(bottom).addClass('stuck');
                        }
                    }
                });
            }
    		
    		if (bottom.length) {
                $(bottom).addClass('stuck');
            }
    	},
    };
    

    $(document).ready(function () {
    	Main.init();
    });

}(window.jQuery);
